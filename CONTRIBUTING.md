# Contributing

This repository makes use of the __Git Flow__. When making contribution to the repository, please make sure the appropriate __Git Flow__ is followed as described below.

## Versioning

This repository makes use of [Semantic Versioning](https://semver.org/)

Core concept being ```MAJOR.MINOR.PATCH```

## Standard and Ways of working 

* Use feature branches rather than direct commits on the main branch.
* Merge frequesntly, it is easier and quicker for people to review and concice code
* Lint and test your code first, before you present it to the world
* Everyone starts from development and targets main.
* Have communication with peers working on the same project
* Commit messages reflect intent.
* Be concise and clear on the documentaion of the code, especially for the code that is not self explanatory

## Pull request process

### Feature pull request
### Feature pull request

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/