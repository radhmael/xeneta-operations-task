# XENETA-OPERATIONS-TASK

## Overview

This is a deployment scenario for [xeneta-operational-task](https://github.com/xeneta/operations-task). 
This project will build the necessary underlying infrastructure and a containerised way to run the application

## Implementaion 

![Image](images/high_level.jpg)

The solution was built on AWS using Gitlab as the CI/CD service. 
The following lists the  core tools/technology consumed 

- AWS 
- gitlab
- Terraform
- docker 

The solution is implemented using Terraform and broken down to three core modules. 
Module level documentation can be found in each module.

- [network-resources](terraform/services/network-resources/)
    -   This module will provision a new VPC with a 3 tier network model 
    across 3 AZs and the associated resources such as RTs SGs and NAT GWs
         - public - hosting the ALB facing the internet
         - private - hosting the ECS cluster
         - database - hosting the Database resources
- [ecs-core-components](terraform/services/ecs-core-components/)
    -   This module creates the necessary resources for the ECS Service such as the ECR, 
    for pushing images, Database, Secrets for accessing the DB passwords, 
    ECS cluster and the task definitions
- [ecs-service](terraform/services/ecs-service/)
    -   This module creates the ALB, discovery service, 
    the ECS service and the adds the autoscaling targets.

## Steps to reproduce

- you need to have a gitlab account and an AWS account. 
- follow the docs in gitlab to [setup AWS creds as masked vales](https://docs.gitlab.com/ee/ci/variables/) 
and [use gitlab as a http backend](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) 
to setup access to your environment
- fork the project and run ! 
- The variables are stored under deployment_variables as `{stage_name}-{environment}.tfvars`. 
Populate as required
- run from the CICD -> pipelines window. 
- All stages run by default validating and doing a plan of the code,
while apply and destroy are manual actions. 
- Once deployed, check the console output of the `ecs-service-apply` job to get the DNS of the ALB 
to connect to. You should get a hello world response

```
{
  "message": "Hello world!"
}
```
after which you can connect to the rates endpoint as described in the original README

```
http://{ALB_DNS_NAME}/rates?date_from=2021-01-01&date_to=2021-01-31&orig_code=CNGGZ&dest_code=EETLL

{
  "rates": [
    {
      "count": 411,
      "day": "2021-01-31",
      "price": 1154.3333333333333
    },
    {
      "count": 411,
      "day": "2021-01-30",
      "price": 1154.3333333333333
    }
.... 
  ]
}

```



## CI / CD

Infrastructure is orchestraed using gitlab.
- some points to note
    - The sensitive variables for the build are stored as evironamental variables 
    at the project level and masked as described in [the official documentation](https://docs.gitlab.com/ee/ci/variables/)
    - Gitlab is used as a http_backend eliminating the creation of a seperate 
    state management service as described in [the official documentation](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
    - Terraform code is validated and scanned on a merge request being opened

## Architecture Decisions

 - Gitlab was chosen since it eliminates most of the overhead like state management and setting up seperate CI CD
 - The applciation is hosted accross three AZs in the ap-southeast1 region to provide High avaialbility
 - Resources created follow implicit and explcit tagging to allow room for easy management and reporting 
 - deployment is done using a seperate IAM user for deployment activities
 - Setup using a three tier architecture for better isolation
 - Resources are encrpted based on requirement.
 - Database password is stored in secret manager as a JSON for easy access and management
 - Database is setup with read replicas to maintain HA
 - ECS is used with Fargate as the capacity provider to reduce management overhead and fits the requirement

## Points for improvement 

 - At the time of  initial configuration all the variables are stored as default tfvars in the module directoris so no variables are generated or passed during the actual run. This can be improved by having a common config store either on a seperate repository or a database and retrieved on execution. 
 - Tried to integrate with DD, was not able to complete
 - Need to do a security review 
 - create branch specific actions on gitlab to automate deployment cycle
 - we can add a WAF infront of the ALB to make sure we get of unwanted traffic
 - we can associate the ALB and the NAT EIPs with shield advanced

## Data ingestion pipeline

- In order to ingest data we can follow an approach as discussed below

- Assumptions
  - A secure mechanism exists to upload files into S3 conforming to a similar format.
  - The source file structure is unaltered.

### Target Architecure

![Image](images/data-ingestion.jpg)

### Data Flow
![Image](images/data_flow.jpg)

- The solution can be implemented by breaking it down to smaller epics
  - Analyze source systems for data structure and attributes.	
  - Define the partition and access strategy
  - Create IAM and secrets for necessry services
  - Launch RDS service with read replicas and HA in mind 
  - Create necessary resources in Glue Data Catalog to crawl data from s3, process and load it to the 
  target endpoints
  - connect the processed S3 to athena/ dashboaring service
  - setup monitors and dashboard to alert the necessry teams
  
