
##common variables
application_id      = "XENOPS"
aws_region          = "ap-southeast-1"
cost_center         = "CC001"
data_classification = "INTERNAL"
department          = "PP"
owner               = "droidxv2@gmail.com"
ecr_details = [
  {
    name                 = "rates-application",
    force_delete         = true,
    image_tag_mutability = "MUTABLE",
    scan_on_push         = false
  },
  {
    name                 = "db-init-job",
    force_delete         = true,
    image_tag_mutability = "MUTABLE",
    scan_on_push         = false
  }
]

cluster_details = [
  {
    name                          = "XENOPS"
    logging                       = "OVERRIDE"
    container_insights_setting    = "disabled"
    default_ecs_capacity_provider = "FARGATE"
    ecs_capacity_providers        = ["FARGATE"]
  }
]

databse_details = [
  {
    name                     = "postgres_db"
    vpc_name                 = "XENOPS"
    allocated_storage        = 5
    backup_retention_period  = 3
    backup_window            = "09:46-10:16"
    delete_automated_backups = true
    engine                   = "postgres"
    engine_version           = "13.5"
    instance_class           = "db.t3.micro"
    maintenance_window       = "Mon:00:00-Mon:03:00"
    publicly_accessible      = false
    skip_final_snapshot      = true
    storage_encrypted        = true
    username                 = "postgres"
    read_replica_count       = 1
  }
]

rates_app_task_name             = "rates-application-task-definition"
rates_container_definition_name = "rates-application"
rates_container_port            = 3000

db_init_task_name                 = "db-init-application-task-definition"
db_init_container_definition_name = "db-init-application"
cpu                               = 256
memory                            = 512
skip_destroy                      = true


