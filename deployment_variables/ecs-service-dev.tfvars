##common variables
application_id      = "XENOPS"
aws_region          = "ap-southeast-1"
cost_center         = "CC001"
data_classification = "INTERNAL"
department          = "PP"
owner               = "droidxv2@gmail.com"
ecs_service_details = [
  {
    app_container_definition_name          = null
    app_container_port                     = null
    app_deployment_maximum_percent         = 200
    app_deployment_minimum_healthy_percent = null
    app_desired_count                      = 1
    service_name                           = "db-init-application-service"
    task_definition_name                   = "db-init-application-task-definition"
    launch_type                            = "FARGATE"
    service_registries                     = null
    load_balancer                          = null
  },
  {
    app_container_definition_name          = "rates-application"
    app_container_port                     = 3000
    app_deployment_maximum_percent         = 200
    app_deployment_minimum_healthy_percent = 50
    app_desired_count                      = 3
    service_name                           = "rates-application-service"
    task_definition_name                   = "rates-application-task-definition"
    launch_type                            = "FARGATE"
    service_registries                     = 1
    load_balancer                          = 1
  }
]

load_balancer_details = [
  {
    enable_deletion_protection = false
    health_check_interval      = 5
    health_check_path          = "/"
    health_check_port          = 3000
    health_check_protocol      = "HTTP"
    health_check_timeout       = 3
    healthy_threshold          = 3
    internal                   = false
    listener_default_action    = "forward"
    listener_port              = 80
    listener_protocol          = "HTTP"
    name                       = "rates"
    target_group_port          = 3000
    target_group_type          = "ip"
    type                       = "application"
    unhealthy_threshold        = 2
  }
]

ecs_autoscaling_details = {
  max_capacity                = 4
  min_capacity                = 1
  cpu_threshold               = 75
  cool_down_time              = 300
  target_tracking_policy_name = "ecs-rates-app-scaling-policy"
  name                        = "rates-application-service"
}