SHELL := /bin/bash
export AWS_DEFAULT_REGION="ap-southeast-1"moduleName := `basename "$(PWD)" | tr 'a-z' 'A-Z'`
root_dir := $(dir $(abspath $(firstword $(MAKEFILE_LIST))))


terraform_modules :=	terraform/modules/aws_ecr_repository/ \
						terraform/modules/cloudwatch/ \
						terraform/modules/database_resources/ \
						terraform/modules/ecs/ \
						terraform/modules/ecs-task-definition/ \
						terraform/modules/iam-assume-role/ \
						terraform/modules/internet_gateway/ \
						terraform/modules/kms_key/ \
						terraform/modules/load_balancer/ \
						terraform/modules/managed_sg_rule/ \
						terraform/modules/nat_gateway/ \
						terraform/modules/rds_databse/ \
						terraform/modules/route/ \
						terraform/modules/route_table_association/ \
						terraform/modules/route_tables/ \
						terraform/modules/security_group/ \
						terraform/modules/security_group_rule_list/ \
						terraform/modules/subnet/ \
						terraform/modules/task-definitions/ \
						terraform/modules/vpc/ \

terraform_services :=	terraform/services/network-resources/ \
						terraform/services/ecs-core-components/ \
						terraform/services/ecs-service/ \

all: terraform-fmt terraform-vld terraform-docs

terraform-fmt:
	echo ""
	echo "Performaing terraform format on module $(root_dir)"
	terraform fmt -recursive
terraform-vld:	
	@echo ""
	for i in $(terraform_services); do \
		echo "Performaing Validation on $$i";\
		cd $$i; \
		terraform init -backend=False; \
		terraform validate; \
		cd $(root_dir); \
	done;
terraform-docs:
	for i in $(terraform_services); do \
		echo ""; \
		echo "Updating terraform documentation on the $$i module"; \
		echo ""; \
		printf "# $(moduleName)\n\n## Module Development Standards\n\n[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)\n\n $$(terraform-docs markdown $$i)" > $$i/README\.md; \
		printf "\n\n## hcl .tfvars file format" >> $$i/README\.md; \
		printf '\n\n```\n' >> $$i/README\.md; \
		printf "$$(terraform-docs tfvars hcl $$i)" >> $$i/README\.md; \
		printf '\n\n```\n' >> $$i/README\.md; \
	done;

	for i in $(terraform_modules); do \
		echo ""; \
		echo "Updating terraform documentation on the $$i module"; \
		echo ""; \
		printf "# $(moduleName)\n\n## Module Development Standards\n\n[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)\n\n $$(terraform-docs markdown $$i)" > $$i/README\.md; \
		printf "\n\n## hcl .tfvars file format" >> $$i/README\.md; \
		printf '\n\n```\n' >> $$i/README\.md; \
		printf "$$(terraform-docs tfvars hcl $$i)" >> $$i/README\.md; \
		printf '\n\n```\n' >> $$i/README\.md; \
	done;
