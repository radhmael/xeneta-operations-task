# ECR
resource "aws_ecr_repository" "ecr_repository" {
  name                 = var.container_repository_name
  image_tag_mutability = var.image_tag_mutability
  force_delete         = var.force_delete
  encryption_configuration {
    encryption_type = "AES256"
  }

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }

  tags = merge(var.common_tags,
    {
      Name = var.container_repository_name
    }
  )
}

# # ECR resource policy
resource "aws_ecr_repository_policy" "ecr_repository_policy" {
  repository = aws_ecr_repository.ecr_repository.name
  policy     = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "ecr policy",
            "Effect": "Allow",
            "Principal": 
              {
                "AWS": 
                  [
                    "arn:aws:iam::440713360388:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
                  ]
              },
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
    ]
}
EOF
}