output "registry_id" {
  description = "The registry ID where the repository was created."
  value = {
    "name" = var.container_repository_name
    "id"   = aws_ecr_repository.ecr_repository.registry_id
  }
}

output "repository_url" {
  description = "The URL of the repository"
  value = {
    "name" = var.container_repository_name
    "url"  = aws_ecr_repository.ecr_repository.repository_url
  }
}