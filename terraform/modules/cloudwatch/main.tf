module "cloudwatch_log_group" {
  source            = "./modules/log-groups/"
  count             = var.create_log_group ? 1 : 0
  name              = var.name
  name_prefix       = var.name_prefix
  retention_in_days = var.retention_in_days
  kms_key_id        = var.kms_key_id

  tags = var.tags
}