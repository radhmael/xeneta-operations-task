# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_kms_key"></a> [kms\_key](#module\_kms\_key) | ../kms_key | n/a |
| <a name="module_rds_databse"></a> [rds\_databse](#module\_rds\_databse) | ../rds_databse | n/a |
| <a name="module_rds_databse_replica"></a> [rds\_databse\_replica](#module\_rds\_databse\_replica) | ../rds_databse | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_db_subnet_group.database_subnet_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group) | resource |
| [aws_secretsmanager_secret.database_password](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_policy.secret_access_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_policy) | resource |
| [aws_secretsmanager_secret_version.database_password](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [random_string.random_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allocated_storage"></a> [allocated\_storage](#input\_allocated\_storage) | allocated\_storage | `any` | n/a | yes |
| <a name="input_availability_zone_list"></a> [availability\_zone\_list](#input\_availability\_zone\_list) | availability\_zone\_list | `any` | n/a | yes |
| <a name="input_backup_retention_period"></a> [backup\_retention\_period](#input\_backup\_retention\_period) | backup\_retention\_period | `any` | n/a | yes |
| <a name="input_backup_window"></a> [backup\_window](#input\_backup\_window) | backup\_window | `any` | n/a | yes |
| <a name="input_common_tags"></a> [common\_tags](#input\_common\_tags) | tags to be associated | `any` | n/a | yes |
| <a name="input_database_subnet_ids"></a> [database\_subnet\_ids](#input\_database\_subnet\_ids) | database\_subnet\_ids | `any` | n/a | yes |
| <a name="input_databse_name"></a> [databse\_name](#input\_databse\_name) | name of the databse to be created | `any` | n/a | yes |
| <a name="input_delete_automated_backups"></a> [delete\_automated\_backups](#input\_delete\_automated\_backups) | delete\_automated\_backups | `any` | n/a | yes |
| <a name="input_engine"></a> [engine](#input\_engine) | engine | `any` | n/a | yes |
| <a name="input_engine_version"></a> [engine\_version](#input\_engine\_version) | engine\_version | `any` | n/a | yes |
| <a name="input_identifier"></a> [identifier](#input\_identifier) | identifier | `any` | n/a | yes |
| <a name="input_instance_class"></a> [instance\_class](#input\_instance\_class) | instance\_class | `any` | n/a | yes |
| <a name="input_maintenance_window"></a> [maintenance\_window](#input\_maintenance\_window) | maintenance\_window | `any` | n/a | yes |
| <a name="input_publicly_accessible"></a> [publicly\_accessible](#input\_publicly\_accessible) | publicly\_accessible | `any` | n/a | yes |
| <a name="input_read_replica_count"></a> [read\_replica\_count](#input\_read\_replica\_count) | read\_replica\_count | `any` | n/a | yes |
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | resource\_prefix | `any` | n/a | yes |
| <a name="input_secret_manager_access_policy"></a> [secret\_manager\_access\_policy](#input\_secret\_manager\_access\_policy) | a json poliy to attach the secret password | `any` | n/a | yes |
| <a name="input_skip_final_snapshot"></a> [skip\_final\_snapshot](#input\_skip\_final\_snapshot) | skip\_final\_snapshot | `any` | n/a | yes |
| <a name="input_storage_encrypted"></a> [storage\_encrypted](#input\_storage\_encrypted) | storage\_encrypted | `any` | n/a | yes |
| <a name="input_username"></a> [username](#input\_username) | username | `any` | n/a | yes |
| <a name="input_vpc_security_group_ids"></a> [vpc\_security\_group\_ids](#input\_vpc\_security\_group\_ids) | vpc\_security\_group\_ids | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_database_endpoint"></a> [database\_endpoint](#output\_database\_endpoint) | n/a |
| <a name="output_database_identifier"></a> [database\_identifier](#output\_database\_identifier) | n/a |
| <a name="output_database_password_arn"></a> [database\_password\_arn](#output\_database\_password\_arn) | n/a |
| <a name="output_database_username"></a> [database\_username](#output\_database\_username) | n/a |
| <a name="output_kms_key_id"></a> [kms\_key\_id](#output\_kms\_key\_id) | n/a |

## hcl .tfvars file format

```
allocated_storage            = ""
availability_zone_list       = ""
backup_retention_period      = ""
backup_window                = ""
common_tags                  = ""
database_subnet_ids          = ""
databse_name                 = ""
delete_automated_backups     = ""
engine                       = ""
engine_version               = ""
identifier                   = ""
instance_class               = ""
maintenance_window           = ""
publicly_accessible          = ""
read_replica_count           = ""
resource_prefix              = ""
secret_manager_access_policy = ""
skip_final_snapshot          = ""
storage_encrypted            = ""
username                     = ""
vpc_security_group_ids       = ""

```
