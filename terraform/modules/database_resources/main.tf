module "kms_key" {
  source                   = "../kms_key"
  key_name                 = format("%s-database-kms-key", var.databse_name)
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = "7"
  description              = format("KEY for encrypting the %s databse", var.databse_name)
  enable_key_rotation      = true
  key_usage                = "ENCRYPT_DECRYPT"
  tags                     = var.common_tags
}

resource "random_string" "random_password" {
  length  = 24
  upper   = true
  numeric = true
  special = false
}

resource "aws_secretsmanager_secret" "database_password" {
  name = format("%s-password_0", var.identifier)
}

resource "aws_secretsmanager_secret_policy" "secret_access_policy" {
  secret_arn = aws_secretsmanager_secret.database_password.arn
  policy     = var.secret_manager_access_policy
}

# The subnets to be used for DB instances 
resource "aws_db_subnet_group" "database_subnet_group" {
  name       = format("%s-database-subnet-group", var.resource_prefix)
  subnet_ids = var.database_subnet_ids

  tags = {
    Name = format("%s-database-subnet-group", var.resource_prefix)
  }
}

module "rds_databse" {
  source                   = "../rds_databse"
  allocated_storage        = var.allocated_storage
  availability_zone        = var.availability_zone_list[0]
  backup_retention_period  = var.backup_retention_period
  backup_window            = var.backup_window
  db_subnet_group_name     = aws_db_subnet_group.database_subnet_group.name
  delete_automated_backups = var.delete_automated_backups
  engine                   = var.engine
  engine_version           = var.engine_version
  identifier               = var.identifier
  instance_class           = var.instance_class
  kms_key_id               = module.kms_key.key_arn
  maintenance_window       = var.maintenance_window
  password                 = random_string.random_password.result
  publicly_accessible      = var.publicly_accessible
  replicate_source_db      = null
  skip_final_snapshot      = true
  storage_encrypted        = var.storage_encrypted
  tags                     = var.common_tags
  username                 = var.username
  vpc_security_group_ids   = var.vpc_security_group_ids
}

module "rds_databse_replica" {
  count                  = var.read_replica_count
  source                 = "../rds_databse"
  replicate_source_db    = module.rds_databse.database_identifier
  identifier             = format("%s-read-replica-%s", var.identifier, count.index + 1)
  instance_class         = var.instance_class
  vpc_security_group_ids = var.vpc_security_group_ids
  storage_encrypted      = var.storage_encrypted
  skip_final_snapshot    = var.skip_final_snapshot
  kms_key_id             = module.kms_key.key_arn
  availability_zone      = var.availability_zone_list[count.index]
}



resource "aws_secretsmanager_secret_version" "database_password" {
  secret_id = aws_secretsmanager_secret.database_password.id
  secret_string = jsonencode({
    db_endpoint = module.rds_databse.database_endpoint
    db_username = module.rds_databse.database_username
    db_password = random_string.random_password.result
  })
}