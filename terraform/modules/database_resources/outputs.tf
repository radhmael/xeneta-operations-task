output "kms_key_id" {
  description = "kms_key_id"
  value = module.kms_key.key_id
}
output "database_password_arn" {
  description = "database_password_arn"
  value = aws_secretsmanager_secret.database_password.arn
}
output "database_identifier" {
  description = "database_identifier"
  value = module.rds_databse.database_identifier
}

output "database_username" {
  description = "database_username"
  value = module.rds_databse.database_username
}

output "database_endpoint" {
  description = "database_endpoint"
  value = module.rds_databse.database_endpoint
}