variable "databse_name" {
  description = "name of the databse to be created"
}

variable "common_tags" {
  description = "tags to be associated"
}

variable "resource_prefix" {
  description = "resource_prefix"
}

variable "allocated_storage" {
  description = "allocated_storage"
}

variable "availability_zone_list" {
  description = "availability_zone_list"
}

variable "backup_retention_period" {
  description = "backup_retention_period"
}

variable "backup_window" {
  description = "backup_window"
}

variable "delete_automated_backups" {
  description = "delete_automated_backups"
}

variable "engine" {
  description = "engine"
}

variable "engine_version" {
  description = "engine_version"
}

variable "identifier" {
  description = "identifier"
}

variable "instance_class" {
  description = "instance_class"
}

variable "maintenance_window" {
  description = "maintenance_window"
}

variable "publicly_accessible" {
  description = "publicly_accessible"
}

variable "skip_final_snapshot" {
  description = "skip_final_snapshot"
}

variable "storage_encrypted" {
  description = "storage_encrypted"
}

variable "username" {
  description = "username"
}

variable "vpc_security_group_ids" {
  description = "vpc_security_group_ids"
}

variable "database_subnet_ids" {
  description = "database_subnet_ids"
}

variable "read_replica_count" {
  description = "read_replica_count"
}

variable "secret_manager_access_policy" {
  description = "a json poliy to attach the secret password"
}