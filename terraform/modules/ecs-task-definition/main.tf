resource "aws_ecs_task_definition" "service" {
  family                   = var.family
  requires_compatibilities = var.requires_compatibilities
  network_mode             = var.network_mode
  skip_destroy             = var.skip_destroy
  task_role_arn            = var.task_role_arn
  execution_role_arn       = var.execution_role_arn
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = jsonencode([
    {
      name      = var.container_definition_name
      image     = var.container_image_uri
      cpu       = var.container_cpu
      memory    = var.container_memory
      essential = var.container_essential
      portMappings = [
        {
          containerPort = var.containerPort
        }
      ]

      secrets = [
        {
          valueFrom = format("%s:db_password::", var.db_secret_name_arn)
          name      = "PGPASSWORD"
        },
        {
          valueFrom = format("%s:db_endpoint::", var.db_secret_name_arn)
          name      = "db_endpoint"
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.cloudwatch_log_group_name
          awslogs-region        = var.region
          awslogs-stream-prefix = format("%s-%s-", var.family, var.container_definition_name)
        }
      }
    }

  ])

  tags = var.common_tags

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
}