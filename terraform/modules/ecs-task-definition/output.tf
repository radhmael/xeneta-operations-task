output "key_id" {
  description = "ID of the created kms key"
  value       = aws_kms_key.kms_key.key_id
}

output "key_arn" {
  description = "ARN of the created kms key"
  value       = aws_kms_key.kms_key.arn
}