# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_cloudwatch_log_group"></a> [cloudwatch\_log\_group](#module\_cloudwatch\_log\_group) | ../cloudwatch | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ecs_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster) | resource |
| [aws_ecs_cluster_capacity_providers.capacity_provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster_capacity_providers) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | n/a | `any` | n/a | yes |
| <a name="input_common_tags"></a> [common\_tags](#input\_common\_tags) | n/a | `any` | n/a | yes |
| <a name="input_container_insights_setting"></a> [container\_insights\_setting](#input\_container\_insights\_setting) | n/a | `any` | n/a | yes |
| <a name="input_default_ecs_capacity_provider"></a> [default\_ecs\_capacity\_provider](#input\_default\_ecs\_capacity\_provider) | n/a | `any` | n/a | yes |
| <a name="input_ecs_capacity_providers"></a> [ecs\_capacity\_providers](#input\_ecs\_capacity\_providers) | n/a | `any` | n/a | yes |
| <a name="input_logging"></a> [logging](#input\_logging) | n/a | `any` | n/a | yes |
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | n/a | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_ecs_cluster_capacity_provider_name"></a> [aws\_ecs\_cluster\_capacity\_provider\_name](#output\_aws\_ecs\_cluster\_capacity\_provider\_name) | n/a |
| <a name="output_aws_ecs_cluster_details"></a> [aws\_ecs\_cluster\_details](#output\_aws\_ecs\_cluster\_details) | n/a |
| <a name="output_cloudwatch_log_group_details"></a> [cloudwatch\_log\_group\_details](#output\_cloudwatch\_log\_group\_details) | n/a |

## hcl .tfvars file format

```
cluster_name                  = ""
common_tags                   = ""
container_insights_setting    = ""
default_ecs_capacity_provider = ""
ecs_capacity_providers        = ""
logging                       = ""
resource_prefix               = ""

```
