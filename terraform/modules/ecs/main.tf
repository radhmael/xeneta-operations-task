module "cloudwatch_log_group" {
  source            = "../cloudwatch"
  name              = format("%s-%s-log-group", var.resource_prefix, var.cluster_name)
  retention_in_days = 7

  tags = var.common_tags
}

resource "aws_ecs_cluster" "cluster" {
  name = format("%s-%s-cluster", var.resource_prefix, var.cluster_name)

  setting {
    name  = "containerInsights"
    value = var.container_insights_setting
  }

  configuration {
    execute_command_configuration {
      logging = var.logging

      log_configuration {
        cloud_watch_log_group_name = module.cloudwatch_log_group.cloudwatch_log_group_name[0]
      }
    }
  }

  tags = merge(
    var.common_tags,
    {
      "Name" = format("%s-%s-cluster", var.resource_prefix, var.cluster_name)
    }

  )

}

resource "aws_ecs_cluster_capacity_providers" "capacity_provider" {
  cluster_name = format("%s-%s-cluster", var.resource_prefix, var.cluster_name)

  capacity_providers = var.ecs_capacity_providers

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = var.default_ecs_capacity_provider
  }

  depends_on = [
    aws_ecs_cluster.cluster
  ]

}