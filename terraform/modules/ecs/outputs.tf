output "cloudwatch_log_group_details" {
  description = "cloudwatch_log_group_details"
  value = {
    "name" = module.cloudwatch_log_group.cloudwatch_log_group_name[0]
    "arn"  = module.cloudwatch_log_group.cloudwatch_log_group_arn[0]
  }
}
output "aws_ecs_cluster_details" {
  description = "aws_ecs_cluster_details"
  value = {
    "name" = aws_ecs_cluster.cluster.tags_all.Name,
    "id"   = aws_ecs_cluster.cluster.id
  }
}

output "aws_ecs_cluster_capacity_provider_name" {
  description = "capacity_provider_name"
  value = aws_ecs_cluster_capacity_providers.capacity_provider.id
}
