variable "resource_prefix" {
    description = "resource_prefix"

}

variable "common_tags" {
    description = "common_tags"

}

variable "logging" {

    description = "logging"
}

variable "container_insights_setting" {
    description = "container_insights_setting"

}

variable "ecs_capacity_providers" {
    description = "ecs_capacity_providers"

}

variable "default_ecs_capacity_provider" {

}

variable "cluster_name" {

}