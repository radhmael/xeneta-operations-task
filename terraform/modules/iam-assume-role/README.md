# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_iam_assume_role"></a> [iam\_assume\_role](#module\_iam\_assume\_role) | ./modules/iam_assume_role/ | n/a |
| <a name="module_iam_policy"></a> [iam\_policy](#module\_iam\_policy) | ./modules/iam_policy/ | n/a |
| <a name="module_instance_profile"></a> [instance\_profile](#module\_instance\_profile) | ./modules/iam_instance_profile/ | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_permission_boundry_policy_arn"></a> [assume\_role\_permission\_boundry\_policy\_arn](#input\_assume\_role\_permission\_boundry\_policy\_arn) | arn of the permisison boundry policy | `string` | `null` | no |
| <a name="input_assume_role_policies"></a> [assume\_role\_policies](#input\_assume\_role\_policies) | list of policies to be attahed to the role | `list(string)` | `[]` | no |
| <a name="input_assume_role_policy"></a> [assume\_role\_policy](#input\_assume\_role\_policy) | assume role polocy attached to the role | `any` | `null` | no |
| <a name="input_attach_created_iam_policy"></a> [attach\_created\_iam\_policy](#input\_attach\_created\_iam\_policy) | should the iam policy created be attahced to the policy | `bool` | `false` | no |
| <a name="input_aws_account_name"></a> [aws\_account\_name](#input\_aws\_account\_name) | name of the aws account where resource is being provisoned | `string` | `"aws-lv-master-nonprod"` | no |
| <a name="input_create_iam_polcy"></a> [create\_iam\_polcy](#input\_create\_iam\_polcy) | do you want to create iam policy | `bool` | `false` | no |
| <a name="input_create_iam_role"></a> [create\_iam\_role](#input\_create\_iam\_role) | do you want to create a IAM role | `bool` | `false` | no |
| <a name="input_create_instance_profile"></a> [create\_instance\_profile](#input\_create\_instance\_profile) | do you want to create iam policy | `bool` | `false` | no |
| <a name="input_iam_policies"></a> [iam\_policies](#input\_iam\_policies) | A list of policies to be created | <pre>list(object(<br>    {<br>      name        = any<br>      description = any<br>      policy      = any<br>      path        = any<br>    }<br>  ))</pre> | `[]` | no |
| <a name="input_max_session_duration"></a> [max\_session\_duration](#input\_max\_session\_duration) | the maximum session duration for the role | `number` | `3600` | no |
| <a name="input_role_name"></a> [role\_name](#input\_role\_name) | name of the IAM role to be created | `string` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | list of tags to be attched to the resources created | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_profile_arn"></a> [instance\_profile\_arn](#output\_instance\_profile\_arn) | name of the role created |
| <a name="output_instance_profile_id"></a> [instance\_profile\_id](#output\_instance\_profile\_id) | name of the role created |
| <a name="output_instance_profile_unique_id"></a> [instance\_profile\_unique\_id](#output\_instance\_profile\_unique\_id) | name of the role created |
| <a name="output_policy_arn"></a> [policy\_arn](#output\_policy\_arn) | list of policy arns created |
| <a name="output_policy_name"></a> [policy\_name](#output\_policy\_name) | list of policy names created |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | name of the role created |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | name of the role created |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | name of the role created |

## hcl .tfvars file format

```
assume_role_permission_boundry_policy_arn = ""
assume_role_policies                      = []
assume_role_policy                        = ""
attach_created_iam_policy                 = false
aws_account_name                          = "aws-lv-master-nonprod"
create_iam_polcy                          = false
create_iam_role                           = false
create_instance_profile                   = false
iam_policies                              = []
max_session_duration                      = 3600
role_name                                 = false
tags                                      = {}

```
