locals {
  tenent_name  = element(split("-", var.aws_account_name), 1)
  project_name = element(split("-", var.aws_account_name), 2)
  account_env  = element(split("-", var.aws_account_name), 3)

  resource_prefix       = lower(format("%s-%s-%s", local.tenent_name, local.project_name, local.account_env))
  instance_profile_name = var.create_instance_profile ? format("%s-instance-profile", var.role_name) : null
}