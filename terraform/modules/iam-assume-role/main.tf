module "iam_policy" {
  count        = var.create_iam_polcy ? 1 : 0
  source       = "./modules/iam_policy/"
  iam_policies = var.iam_policies
}

module "iam_assume_role" {
  count                = var.create_iam_role ? 1 : 0
  source               = "./modules/iam_assume_role/"
  role_name            = var.role_name
  assume_role_policy   = var.assume_role_policy
  permissions_boundary = var.assume_role_permission_boundry_policy_arn
  common_tags          = var.tags
  policies_to_attach   = var.assume_role_policies
  max_session_duration = var.max_session_duration
  depends_on           = [module.iam_policy]
}

module "instance_profile" {
  count                 = var.create_instance_profile ? 1 : 0
  source                = "./modules/iam_instance_profile/"
  role_name             = var.role_name
  instance_profile_name = local.instance_profile_name
  depends_on            = [module.iam_assume_role]
}