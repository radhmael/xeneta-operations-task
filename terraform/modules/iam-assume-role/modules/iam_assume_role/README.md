# IAM-ASSUME-ROLE

## Module Development Standards

[Please refer the development Standards before working on a repo](https://hglw.atlassian.net/wiki/spaces/CD/pages/88080385/Terraform+Development+Standards)

 ## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=0.13.7 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.managed_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.test-attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_role_policy"></a> [assume\_role\_policy](#input\_assume\_role\_policy) | policy for the assume role | `any` | n/a | yes |
| <a name="input_common_tags"></a> [common\_tags](#input\_common\_tags) | a map of the tags to be attached | `map(string)` | n/a | yes |
| <a name="input_max_session_duration"></a> [max\_session\_duration](#input\_max\_session\_duration) | the maximum session duration for the role | `number` | `3600` | no |
| <a name="input_permissions_boundary"></a> [permissions\_boundary](#input\_permissions\_boundary) | arn of the permisson boundry to attach | `string` | `null` | no |
| <a name="input_policies_to_attach"></a> [policies\_to\_attach](#input\_policies\_to\_attach) | a list of polices to be attached to the role | `list(string)` | `[]` | no |
| <a name="input_role_name"></a> [role\_name](#input\_role\_name) | name of the iam role to create | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | name of the role created |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | name of the role created |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | name of the role created |

## hcl .tfvars file format

```
assume_role_policy   = ""
common_tags          = ""
max_session_duration = 3600
permissions_boundary = ""
policies_to_attach   = []
role_name            = ""

```
