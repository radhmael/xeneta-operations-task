resource "aws_iam_role" "managed_role" {
  name                 = var.role_name
  assume_role_policy   = var.assume_role_policy
  permissions_boundary = var.permissions_boundary
  max_session_duration = var.max_session_duration
  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "IAMROLE"
      }

    )
  )
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  for_each   = toset(var.policies_to_attach)
  role       = aws_iam_role.managed_role.name
  policy_arn = each.key
  depends_on = [aws_iam_role.managed_role]
}
