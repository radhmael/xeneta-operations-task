output "role_name" {
  description = "name of the role created"
  value       = aws_iam_role.managed_role.name
}

output "role_id" {
  description = "name of the role created"
  value       = aws_iam_role.managed_role.id
}

output "role_arn" {
  description = "name of the role created"
  value       = aws_iam_role.managed_role.arn
}