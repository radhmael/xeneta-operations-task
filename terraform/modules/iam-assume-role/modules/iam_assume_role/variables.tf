variable "role_name" {
  description = "name of the iam role to create"
  type        = string
}

variable "assume_role_policy" {
  description = "policy for the assume role"
  type        = any
}

variable "permissions_boundary" {
  description = "arn of the permisson boundry to attach"
  type        = string
  default     = null
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "policies_to_attach" {
  description = "a list of polices to be attached to the role"
  type        = list(string)
  default     = []
}

variable "max_session_duration" {
  description = "the maximum session duration for the role"
  type        = number
  default     = 3600
}