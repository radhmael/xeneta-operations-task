output "instance_profile_unique_id" {
  description = "name of the role created"
  value       = aws_iam_instance_profile.instance_profile.unique_id
}

output "instance_profile_id" {
  description = "name of the role created"
  value       = aws_iam_instance_profile.instance_profile.id
}

output "instance_profile_arn" {
  description = "name of the role created"
  value       = aws_iam_instance_profile.instance_profile.arn
}