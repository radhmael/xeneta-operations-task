variable "instance_profile_name" {
  description = "instance_profile_name"
  type        = string
}

variable "role_name" {
  description = "name of the iam role to create the profile for"
  type        = string
}