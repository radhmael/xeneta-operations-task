# IAM-ASSUME-ROLE

## Module Development Standards

[Please refer the development Standards before working on a repo](https://hglw.atlassian.net/wiki/spaces/CD/pages/88080385/Terraform+Development+Standards)

 ## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=0.13.7 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_iam_policies"></a> [iam\_policies](#input\_iam\_policies) | A list of policies to be created | <pre>list(object(<br>    {<br>      name        = any<br>      description = any<br>      policy      = any<br>      path        = any<br>    }<br>  ))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_policy_arn"></a> [policy\_arn](#output\_policy\_arn) | list of policy arns created |
| <a name="output_policy_name"></a> [policy\_name](#output\_policy\_name) | list of policy names created |

## hcl .tfvars file format

```
iam_policies = []

```
