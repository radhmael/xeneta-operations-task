resource "aws_iam_policy" "policy" {
  for_each    = { for policy in var.iam_policies : policy.name => policy }
  name        = each.value.name
  path        = each.value.path
  description = each.value.description
  policy      = each.value.policy
}