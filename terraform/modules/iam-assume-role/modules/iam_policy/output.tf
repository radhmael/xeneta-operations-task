output "policy_name" {
  description = "list of policy names created"
  value = [
    for policy in aws_iam_policy.policy : policy.name
  ]
}

output "policy_arn" {
  description = "list of policy arns created"
  value = [
    for policy in aws_iam_policy.policy : policy.arn
  ]
}