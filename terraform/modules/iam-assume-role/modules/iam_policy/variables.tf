variable "iam_policies" {
  description = "A list of policies to be created"
  type = list(object(
    {
      name        = any
      description = any
      policy      = any
      path        = any
    }
  ))
  default = []
}