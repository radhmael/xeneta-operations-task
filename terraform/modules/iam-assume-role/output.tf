#### IAM policy ####

output "policy_name" {
  description = "list of policy names created"
  value       = var.create_iam_polcy ? module.iam_policy[*].policy_name[*] : null
}

output "policy_arn" {
  description = "list of policy arns created"
  value       = var.create_iam_polcy ? module.iam_policy[*].policy_arn[*] : null
}

####  IAM Role  ####

output "role_name" {
  description = "name of the role created"
  value       = var.create_iam_role ? join("", module.iam_assume_role.*.role_name) : null
}

output "role_id" {
  description = "name of the role created"
  value       = var.create_iam_role ? join("", module.iam_assume_role.*.role_id) : null
}

output "role_arn" {
  description = "name of the role created"
  value       = var.create_iam_role ? join("", module.iam_assume_role.*.role_arn) : null
}

### Instance profile

output "instance_profile_unique_id" {
  description = "name of the role created"
  value       = var.create_instance_profile ? join("", module.instance_profile.*.instance_profile_unique_id) : null
}

output "instance_profile_id" {
  description = "name of the role created"
  value       = var.create_instance_profile ? join("", module.instance_profile.*.instance_profile_id) : null
}

output "instance_profile_arn" {
  description = "name of the role created"
  value       = var.create_instance_profile ? join("", module.instance_profile.*.instance_profile_arn) : null
}