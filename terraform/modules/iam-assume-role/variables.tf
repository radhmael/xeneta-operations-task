variable "aws_account_name" {
  description = "name of the aws account where resource is being provisoned"
  default     = "aws-lv-master-nonprod"
}

#### IAM policy ####

variable "create_iam_polcy" {
  description = "do you want to create iam policy"
  type        = bool
  default     = false
}

variable "iam_policies" {
  description = "A list of policies to be created"
  type = list(object(
    {
      name        = any
      description = any
      policy      = any
      path        = any
    }
  ))
  default = []
}

#### IAM Role ####

variable "create_iam_role" {
  description = "do you want to create a IAM role"
  type        = bool
  default     = false
}

variable "role_name" {
  description = "name of the IAM role to be created"
  type        = string
  default     = false
}

variable "attach_created_iam_policy" {
  description = "should the iam policy created be attahced to the policy"
  type        = bool
  default     = false
}

variable "assume_role_policy" {
  description = "assume role polocy attached to the role"
  type        = any
  default     = null
}

variable "assume_role_policies" {
  description = "list of policies to be attahed to the role"
  type        = list(string)
  default     = []
}

variable "assume_role_permission_boundry_policy_arn" {
  description = "arn of the permisison boundry policy"
  type        = string
  default     = null
}

variable "tags" {
  description = "list of tags to be attched to the resources created"
  type        = map(string)
  default     = {}
}

variable "max_session_duration" {
  description = "the maximum session duration for the role"
  type        = number
  default     = 3600
}

variable "create_instance_profile" {
  description = "do you want to create iam policy"
  type        = bool
  default     = false
}