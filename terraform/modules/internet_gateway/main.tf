resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = var.vpc_id
  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "internet_gateway",
      }
    )
  )
}
resource "aws_route" "public_subnet_internet_access" {
  for_each               = var.public_route_table_id_list
  route_table_id         = each.value
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gateway.id
}