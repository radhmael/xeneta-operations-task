output "igw_id" {
  description = "ID of the Internet gateway"
  value       = aws_internet_gateway.internet_gateway.*.id
}