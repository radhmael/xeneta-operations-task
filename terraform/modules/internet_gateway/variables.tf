variable "vpc_id" {
  description = "The VPC ID to create in"
}

variable "public_route_table_id_list" {
  description = "list of route tables attached to public subnets"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}