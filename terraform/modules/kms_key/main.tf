resource "aws_kms_key" "kms_key" {
  description              = var.description
  key_usage                = var.key_usage
  customer_master_key_spec = var.customer_master_key_spec
  policy                   = var.policy
  enable_key_rotation      = var.enable_key_rotation
  deletion_window_in_days  = var.deletion_window_in_days
  tags                     = var.tags
}

resource "aws_kms_alias" "kms_key_alias" {
  name          = format("alias/%s", var.key_name)
  target_key_id = aws_kms_key.kms_key.key_id
}