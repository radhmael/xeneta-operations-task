variable "description" {
  description = "The description of the key as viewed in AWS console"
  default     = "KEY for encryption"
}

variable "key_usage" {
  description = "Specifies the intended use of the key"
  default     = "ENCRYPT_DECRYPT"
}

variable "customer_master_key_spec" {
  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports"
  default     = "SYMMETRIC_DEFAULT"

}

variable "policy" {
  description = "key policy attached to the key"
  default     = null
}

variable "enable_key_rotation" {
  description = "Specifies whether key rotation is enabled"
  default     = true

}

variable "deletion_window_in_days" {
  description = "waiting period befroe a key is deleted"
}

variable "key_name" {
  description = "key_alias for the kms key"
}

variable "tags" {

}