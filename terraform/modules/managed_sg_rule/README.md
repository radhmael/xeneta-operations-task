# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_security_group_rule.managed_sg_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | n/a | `any` | n/a | yes |
| <a name="input_rules_list"></a> [rules\_list](#input\_rules\_list) | n/a | `any` | n/a | yes |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | n/a | `any` | n/a | yes |
| <a name="input_security_group_id_list"></a> [security\_group\_id\_list](#input\_security\_group\_id\_list) | n/a | `any` | n/a | yes |

## Outputs

No outputs.

## hcl .tfvars file format

```
resource_prefix        = ""
rules_list             = ""
security_group_id      = ""
security_group_id_list = ""

```
