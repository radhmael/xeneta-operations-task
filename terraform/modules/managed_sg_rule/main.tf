resource "aws_security_group_rule" "managed_sg_rule" {
  for_each          = { for rule in var.rules_list : rule.description => rule }
  type              = each.value.type
  from_port         = each.value.from_port
  to_port           = each.value.to_port
  protocol          = each.value.protocol
  security_group_id = var.security_group_id
  cidr_blocks       = each.value.cidr_blocks
  ipv6_cidr_blocks  = each.value.ipv6_cidr_blocks
  description       = each.value.description
  # prefix_list_ids          = each.value.prefix_list_ids
  self                     = each.value.self
  source_security_group_id = each.value.source_security_group_id != null ? lookup(var.security_group_id_list[0], format("%s-%s-security-group", var.resource_prefix, each.value.source_security_group_id)) : null
}