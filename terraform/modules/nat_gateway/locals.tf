locals {
  resource_prefix = replace(var.subnet_name, "public", "private")
}