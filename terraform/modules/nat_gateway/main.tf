
resource "aws_eip" "nat_gateway_eip" {
  vpc = true
  tags = merge(var.common_tags,
    {
      Name = format("%s-natgw-eip", var.subnet_name)
  })
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_gateway_eip.id
  subnet_id     = var.subnet_id
  tags = merge(var.common_tags,
    {
      Name = format("%s-natgw", var.subnet_name)
  })

}

resource "aws_route" "private_subnet_internet_access" {
  route_table_id         = lookup(var.private_route_table_details, format("%s-route-table", local.resource_prefix))
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
}