output "eip_allocation_id" {
  value = {
    "name" = aws_eip.nat_gateway_eip.tags_all.Name,
    "id"   = aws_eip.nat_gateway_eip.id
  }
}
output "nat_gateway_id" {
  value = {
    "name" = aws_nat_gateway.nat_gateway.tags_all.Name,
    "id"   = aws_nat_gateway.nat_gateway.id
  }
}
