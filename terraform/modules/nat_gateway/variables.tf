variable "subnet_name" {
  description = "name of the subnet where resource is being created"
}

variable "subnet_id" {
  description = "ID of the subnet where resource is being created"
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "resource_prefix" {
  description = "resource_prefix for created resources"
}

variable "private_route_table_details" {
  description = "a map of route table and IDs"
}