resource "aws_db_instance" "rds_instance" {
  allocated_storage        = var.allocated_storage
  replicate_source_db      = var.replicate_source_db
  engine                   = var.engine
  engine_version           = var.engine_version
  instance_class           = var.instance_class
  identifier               = var.identifier
  username                 = var.username
  db_subnet_group_name     = var.db_subnet_group_name
  password                 = var.password
  backup_retention_period  = var.backup_retention_period
  backup_window            = var.backup_window
  maintenance_window       = var.maintenance_window
  delete_automated_backups = var.delete_automated_backups
  vpc_security_group_ids   = var.vpc_security_group_ids
  skip_final_snapshot      = var.skip_final_snapshot
  publicly_accessible      = var.publicly_accessible
  storage_encrypted        = var.storage_encrypted
  availability_zone        = var.availability_zone
  kms_key_id               = var.kms_key_id

  tags = var.tags
}