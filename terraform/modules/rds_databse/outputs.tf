output "database_identifier" {
  value = aws_db_instance.rds_instance.identifier
}

output "database_username" {
  value = aws_db_instance.rds_instance.username
}

output "database_endpoint" {
  value = aws_db_instance.rds_instance.address
}