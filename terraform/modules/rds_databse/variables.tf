variable "allocated_storage" {
  description = "The allocated storage in gibibytes"
  default     = null
}

variable "replicate_source_db" {
  description = " Specifies that this resource is a Replicate database, and to use this value as the source database."
  default     = null
}

variable "engine" {
  description = "The database engine to use"
  default     = null
}

variable "engine_version" {
  description = " The engine version to use"
  default     = null
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  default     = null
}

variable "identifier" {
  description = "The name of the RDS instance"
}

variable "username" {
  description = "Username for the master DB use"
  default     = null
}

variable "db_subnet_group_name" {
  description = "Name of DB subnet group. DB instance will be created in the VPC associated with the DB subnet group."
  default     = null
}

variable "password" {
  description = "Password for the master DB user"
  default     = null
}

variable "backup_retention_period" {
  description = "The days to retain backups for. Must be between 0 and 35"
  default     = null
}

variable "backup_window" {
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled"
  default     = null
}

variable "maintenance_window" {
  description = "maintenance_window"
  default     = null
}

variable "delete_automated_backups" {
  description = "pecifies whether to remove automated backups immediately after the DB instance is deleted"
  default     = true
}

variable "vpc_security_group_ids" {
  description = " List of VPC security groups to associate"
  default     = null
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted"
  default     = true
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
  default     = false
}

variable "storage_encrypted" {
  description = "Specifies whether the DB instance is encrypted"
  default     = false
}

variable "availability_zone" {
  description = "The AZ for the RDS instance"
  default     = null
}

variable "kms_key_id" {
  description = " The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN."
  default     = null
}

variable "tags" {
  description = "tasg to assocaite with teh resource"
  default     = null
}