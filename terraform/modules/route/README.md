# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route.managed_route](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_carrier_gateway_id"></a> [carrier\_gateway\_id](#input\_carrier\_gateway\_id) | value | `any` | `null` | no |
| <a name="input_core_network_arn"></a> [core\_network\_arn](#input\_core\_network\_arn) | value | `any` | `null` | no |
| <a name="input_destination_cidr_block"></a> [destination\_cidr\_block](#input\_destination\_cidr\_block) | The destination CIDR block. | `any` | `null` | no |
| <a name="input_destination_ipv6_cidr_block"></a> [destination\_ipv6\_cidr\_block](#input\_destination\_ipv6\_cidr\_block) | The destination IPv6 CIDR block. | `any` | `null` | no |
| <a name="input_egress_only_gateway_id"></a> [egress\_only\_gateway\_id](#input\_egress\_only\_gateway\_id) | value | `any` | `null` | no |
| <a name="input_gateway_id"></a> [gateway\_id](#input\_gateway\_id) | value | `any` | `null` | no |
| <a name="input_nat_gateway_id"></a> [nat\_gateway\_id](#input\_nat\_gateway\_id) | Identifier of a VPC NAT gateway. | `any` | `null` | no |
| <a name="input_network_interface_id"></a> [network\_interface\_id](#input\_network\_interface\_id) | value | `any` | `null` | no |
| <a name="input_route_table_id"></a> [route\_table\_id](#input\_route\_table\_id) | The ID of the routing table. | `any` | `null` | no |
| <a name="input_sacsdcsccd"></a> [sacsdcsccd](#input\_sacsdcsccd) | value | `any` | `null` | no |
| <a name="input_transit_gateway_id"></a> [transit\_gateway\_id](#input\_transit\_gateway\_id) | value | `any` | `null` | no |
| <a name="input_vpc_endpoint_id"></a> [vpc\_endpoint\_id](#input\_vpc\_endpoint\_id) | value | `any` | `null` | no |
| <a name="input_vpc_peering_connection_id"></a> [vpc\_peering\_connection\_id](#input\_vpc\_peering\_connection\_id) | value | `any` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_route_details"></a> [aws\_route\_details](#output\_aws\_route\_details) | n/a |

## hcl .tfvars file format

```
carrier_gateway_id          = ""
core_network_arn            = ""
destination_cidr_block      = ""
destination_ipv6_cidr_block = ""
egress_only_gateway_id      = ""
gateway_id                  = ""
nat_gateway_id              = ""
network_interface_id        = ""
route_table_id              = ""
sacsdcsccd                  = ""
transit_gateway_id          = ""
vpc_endpoint_id             = ""
vpc_peering_connection_id   = ""

```
