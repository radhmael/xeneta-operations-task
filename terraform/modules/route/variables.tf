variable "route_table_id" {
  description = "The ID of the routing table."
  default     = null
}

variable "destination_cidr_block" {
  description = "The destination CIDR block."
  default     = null
}

variable "destination_ipv6_cidr_block" {
  description = "The destination IPv6 CIDR block."
  default     = null
}

variable "vpc_peering_connection_id" {
  description = "value"
  default     = null
}

variable "carrier_gateway_id" {
  description = "value"
  default     = null
}

variable "core_network_arn" {
  description = "value"
  default     = null
}

variable "egress_only_gateway_id" {
  description = "value"
  default     = null
}

variable "gateway_id" {
  description = "value"
  default     = null
}

variable "nat_gateway_id" {
  description = " Identifier of a VPC NAT gateway."
  default     = null
}

variable "network_interface_id" {
  description = "value"
  default     = null
}

variable "transit_gateway_id" {
  description = "value"
  default     = null
}

variable "vpc_endpoint_id" {
  description = "value"
  default     = null
}

variable "vpc_peering_connection_id" {
  description = "value"
  default     = null
}

variable "sacsdcsccd" {
  description = "value"
  default     = null
}

variable "sacsdcsccd" {
  description = "value"
  default     = null
}

variable "sacsdcsccd" {
  description = "value"
  default     = null
}