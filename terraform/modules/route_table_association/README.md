# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route_table_association.route_table_association](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_route_table_id_list"></a> [route\_table\_id\_list](#input\_route\_table\_id\_list) | map of rtb name to id | `any` | n/a | yes |
| <a name="input_subnet_id_list"></a> [subnet\_id\_list](#input\_subnet\_id\_list) | map of subnet name to id | `any` | n/a | yes |

## Outputs

No outputs.

## hcl .tfvars file format

```
route_table_id_list = ""
subnet_id_list      = ""

```
