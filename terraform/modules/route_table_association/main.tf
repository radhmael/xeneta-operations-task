resource "aws_route_table_association" "route_table_association" {
  for_each       = var.route_table_id_list
  subnet_id      = lookup(var.subnet_id_list, replace(each.key, "-route-table", ""))
  route_table_id = each.value

}