variable "route_table_id_list" {
  description = "map of rtb name to id"
}

variable "subnet_id_list" {
  description = "map of subnet name to id"
}