# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route_table.route_table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input\_name) | name of the route table | `any` | n/a | yes |
| <a name="input_routes"></a> [routes](#input\_routes) | routing rules, supported all type of gateways | `list` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | tags to be associated | `any` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of the vpc | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_route_table_details"></a> [route\_table\_details](#output\_route\_table\_details) | n/a |

## hcl .tfvars file format

```
name   = ""
routes = []
tags   = ""
vpc_id = ""

```
