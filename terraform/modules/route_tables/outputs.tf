output "route_table_details" {
  value = {
    "name" = aws_route_table.route_table.tags.Name,
    "id"   = aws_route_table.route_table.id
  }
}
