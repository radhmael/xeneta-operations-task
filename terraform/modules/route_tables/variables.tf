variable "vpc_id" {
  description = "ID of the vpc"
}

variable "routes" {
  description = "routing rules, supported all type of gateways"
  default     = []
}

variable "name" {
  description = "name of the route table"
}

variable "tags" {
  description = "tags to be associated"
}