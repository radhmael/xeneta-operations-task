resource "aws_security_group" "security_group" {
  for_each    = var.security_group_configs
  name        = format("%s-%s-security-group", var.resource_prefix, each.key)
  description = format("security-group for the %s tier", each.key)
  vpc_id      = var.vpc_id
  tags = merge(var.tags,
    {
      Name = format("%s-%s-security-group", var.resource_prefix, each.key)
  })
}

