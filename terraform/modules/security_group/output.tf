output "security_group_id_list" {
  value = { for k, v in aws_security_group.security_group : v.tags.Name => v.id }
}