# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_managed_sg_rule"></a> [managed\_sg\_rule](#module\_managed\_sg\_rule) | ../managed_sg_rule | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | n/a | `any` | n/a | yes |
| <a name="input_security_group_details"></a> [security\_group\_details](#input\_security\_group\_details) | n/a | `any` | n/a | yes |
| <a name="input_security_group_id_list"></a> [security\_group\_id\_list](#input\_security\_group\_id\_list) | n/a | `any` | n/a | yes |

## Outputs

No outputs.

## hcl .tfvars file format

```
resource_prefix        = ""
security_group_details = ""
security_group_id_list = ""

```
