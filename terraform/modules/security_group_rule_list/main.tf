module "managed_sg_rule" {
  for_each               = var.security_group_details
  source                 = "../managed_sg_rule"
  security_group_id      = lookup(var.security_group_id_list[0], format("%s-%s-security-group", var.resource_prefix, each.key))
  rules_list             = each.value
  security_group_id_list = var.security_group_id_list
  resource_prefix        = var.resource_prefix
}