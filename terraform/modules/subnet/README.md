# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_route_table"></a> [route\_table](#module\_route\_table) | ../route_tables | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_route_table"></a> [create\_route\_table](#input\_create\_route\_table) | create\_route\_table | `bool` | `false` | no |
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | resource prefix | `any` | n/a | yes |
| <a name="input_subnet_list"></a> [subnet\_list](#input\_subnet\_list) | list of subnet cidrs to be created | `any` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `map` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of the VPC where subnet is created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_route_table_id_list"></a> [route\_table\_id\_list](#output\_route\_table\_id\_list) | n/a |
| <a name="output_subnet_id_list"></a> [subnet\_id\_list](#output\_subnet\_id\_list) | n/a |

## hcl .tfvars file format

```
create_route_table = false
resource_prefix    = ""
subnet_list        = ""
tags               = {}
vpc_id             = ""

```
