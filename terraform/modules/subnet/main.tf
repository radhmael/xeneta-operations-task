resource "aws_subnet" "subnet" {
  for_each                = { for subnet in var.subnet_list : subnet.cidr_range => subnet }
  vpc_id                  = var.vpc_id
  cidr_block              = each.value["cidr_range"]
  map_public_ip_on_launch = contains(split("-", each.value["name"]), "public") ? true : false
  availability_zone       = each.value["availability_zone"]
  tags = merge(each.value["tags"], var.tags, {
    Name = format("%s-%s", var.resource_prefix, each.value["name"])
  })
}

module "route_table" {
  source   = "../route_tables"
  for_each = var.create_route_table == true ? { for subnet in var.subnet_list : subnet.cidr_range => subnet } : {}
  vpc_id   = var.vpc_id
  name     = format("%s-%s-route-table", var.resource_prefix, each.value["name"])
  tags     = var.tags
}

locals {
  subnet_map = { for k, v in aws_subnet.subnet : v.tags.Name => v.id }
}