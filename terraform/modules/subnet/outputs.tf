output "subnet_id_list" {
  value = { for k, v in aws_subnet.subnet : v.tags.Name => v.id }
}
output "route_table_id_list" {
  value = { for k, v in module.route_table : v.route_table_details.name => v.route_table_details.id }
}
