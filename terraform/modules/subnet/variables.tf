variable "subnet_list" {
  description = "list of subnet cidrs to be created"
}

variable "vpc_id" {
  description = "ID of the VPC where subnet is created"
}

variable "resource_prefix" {
  description = "resource prefix"
}

variable "tags" {
  default = {}
}

variable "create_route_table" {
  description = "create_route_table"
  default     = false
}