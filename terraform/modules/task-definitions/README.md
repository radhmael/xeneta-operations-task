# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_execution_role"></a> [execution\_role](#module\_execution\_role) | ../iam-assume-role | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_ecs_task_definition.db-init-task-definition](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |
| [aws_ecs_task_definition.rates-task-definition](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | current aws region | `any` | n/a | yes |
| <a name="input_cloudwatch_log_group_name"></a> [cloudwatch\_log\_group\_name](#input\_cloudwatch\_log\_group\_name) | Name of the Cloudwatch log group | `string` | n/a | yes |
| <a name="input_common_tags"></a> [common\_tags](#input\_common\_tags) | tags to be assocated with the resources | `any` | n/a | yes |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | CPU milicores needed to run the conatiner | `any` | n/a | yes |
| <a name="input_db_init_container_definition_name"></a> [db\_init\_container\_definition\_name](#input\_db\_init\_container\_definition\_name) | Task definition name of the db init job | `string` | n/a | yes |
| <a name="input_db_init_task_name"></a> [db\_init\_task\_name](#input\_db\_init\_task\_name) | Name of the db init application task definition | `string` | n/a | yes |
| <a name="input_db_secret_name_arn"></a> [db\_secret\_name\_arn](#input\_db\_secret\_name\_arn) | ARN of the secret manager secret name | `string` | n/a | yes |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory limit allocated to the container | `any` | n/a | yes |
| <a name="input_rates_app_task_name"></a> [rates\_app\_task\_name](#input\_rates\_app\_task\_name) | Name of the rates application task definition | `string` | n/a | yes |
| <a name="input_rates_container_definition_name"></a> [rates\_container\_definition\_name](#input\_rates\_container\_definition\_name) | Image name of the rates app | `string` | n/a | yes |
| <a name="input_rates_container_port"></a> [rates\_container\_port](#input\_rates\_container\_port) | rates-application container port | `number` | n/a | yes |
| <a name="input_repository_url_map"></a> [repository\_url\_map](#input\_repository\_url\_map) | n/a | `any` | n/a | yes |
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | resource\_prefix | `any` | n/a | yes |
| <a name="input_roles_to_create"></a> [roles\_to\_create](#input\_roles\_to\_create) | n/a | `any` | n/a | yes |
| <a name="input_skip_destroy"></a> [skip\_destroy](#input\_skip\_destroy) | Whether to skip destroying previous version when creating a new one | `bool` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_execution_role_details"></a> [execution\_role\_details](#output\_execution\_role\_details) | n/a |

## hcl .tfvars file format

```
aws_region                        = ""
cloudwatch_log_group_name         = ""
common_tags                       = ""
cpu                               = ""
db_init_container_definition_name = ""
db_init_task_name                 = ""
db_secret_name_arn                = ""
memory                            = ""
rates_app_task_name               = ""
rates_container_definition_name   = ""
rates_container_port              = ""
repository_url_map                = ""
resource_prefix                   = ""
roles_to_create                   = ""
skip_destroy                      = ""

```
