locals {
  iam_role_details = { for k, v in module.execution_role : k => v }
}