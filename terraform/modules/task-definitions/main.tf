module "execution_role" {
  for_each                = { for role in var.roles_to_create : role.role_name => role }
  source                  = "../iam-assume-role"
  create_iam_polcy        = false
  create_iam_role         = true
  create_instance_profile = false
  role_name               = format("%s-%s-iam-role", var.resource_prefix, each.value.role_name)
  assume_role_policy      = each.value.assume_role_policy
  tags                    = var.common_tags
  assume_role_policies    = each.value.polies_to_attach
}

# Task definition for rates application

resource "aws_ecs_task_definition" "rates-task-definition" {
  family                   = var.rates_app_task_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  skip_destroy             = var.skip_destroy
  task_role_arn            = module.execution_role["ecs_task_role"].role_arn
  execution_role_arn       = module.execution_role["ecs_task_execution_role"].role_arn
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = jsonencode([
    {
      name      = var.rates_container_definition_name
      image     = lookup(var.repository_url_map, format("%s-rates-application", var.resource_prefix))
      essential = true

      secrets = [
        {
          valueFrom = format("%s:db_password::", var.db_secret_name_arn)
          name      = "PGPASSWORD"
        },
        {
          valueFrom = format("%s:db_endpoint::", var.db_secret_name_arn)
          name      = "db_endpoint"
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.cloudwatch_log_group_name
          awslogs-region        = var.aws_region
          awslogs-stream-prefix = "rates-"
        }
      }

      portMappings = [
        {
          containerPort = var.rates_container_port
        }
      ]
    }
  ])

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  tags = {
    "Name" = var.rates_app_task_name
  }

}


# Task definition for database initialization job

resource "aws_ecs_task_definition" "db-init-task-definition" {
  family                   = var.db_init_task_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  skip_destroy             = var.skip_destroy
  task_role_arn            = module.execution_role["ecs_task_role"].role_arn
  execution_role_arn       = module.execution_role["ecs_task_execution_role"].role_arn
  cpu                      = var.cpu
  memory                   = var.memory

  container_definitions = jsonencode([
    {
      name      = var.db_init_container_definition_name
      image     = lookup(var.repository_url_map, format("%s-db-init-job", var.resource_prefix))
      essential = true

      secrets = [
        {
          valueFrom = format("%s:db_password::", var.db_secret_name_arn)
          name      = "PGPASSWORD"
        },
        {
          valueFrom = format("%s:db_endpoint::", var.db_secret_name_arn)
          name      = "db_endpoint"
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.cloudwatch_log_group_name
          awslogs-region        = var.aws_region
          awslogs-stream-prefix = "db-init-"
        }
      }


    }
  ])

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  tags = {
    "Name" = var.db_init_container_definition_name
  }

}