###############################################################
# rates-application task definition attributes
variable "rates_app_task_name" {
  description = "Name of the rates application task definition"
  type        = string
}

variable "rates_container_definition_name" {
  description = "Image name of the rates app"
  type        = string
}


variable "rates_container_port" {
  description = "rates-application container port"
  type        = number
}


###############################################################
# db-initialization task definition attributes

variable "db_init_task_name" {
  description = "Name of the db init application task definition"
  type        = string
}

variable "db_init_container_definition_name" {
  description = "Task definition name of the db init job"
  type        = string
}


###############################################################
# Common attributes

variable "cpu" {
  description = "CPU milicores needed to run the conatiner"
}

variable "memory" {
  description = "Memory limit allocated to the container"
}

variable "skip_destroy" {
  description = "Whether to skip destroying previous version when creating a new one"
  type        = bool
}

variable "db_secret_name_arn" {
  description = "ARN of the secret manager secret name"
  type        = string
}

variable "cloudwatch_log_group_name" {
  description = "Name of the Cloudwatch log group"
  type        = string
}

#common variables
variable "resource_prefix" {
  description = "resource_prefix"
}
variable "common_tags" {
  description = "tags to be assocated with the resources"
}

variable "aws_region" {
  description = "current aws region"
}

#IAM roles

variable "roles_to_create" {

}

variable "repository_url_map" {}