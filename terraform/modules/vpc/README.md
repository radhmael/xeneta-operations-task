# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_security_group"></a> [security\_group](#module\_security\_group) | ../security_group/ | n/a |
| <a name="module_subnet"></a> [subnet](#module\_subnet) | ../subnet/ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assign_generated_ipv6_cidr_block"></a> [assign\_generated\_ipv6\_cidr\_block](#input\_assign\_generated\_ipv6\_cidr\_block) | Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block | `bool` | `false` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | aws region where resources are being created in | `string` | n/a | yes |
| <a name="input_cidr_block"></a> [cidr\_block](#input\_cidr\_block) | CIDR block of the VPC | `any` | n/a | yes |
| <a name="input_common_tags"></a> [common\_tags](#input\_common\_tags) | a map of the tags to be attached | `map(string)` | n/a | yes |
| <a name="input_enable_classiclink"></a> [enable\_classiclink](#input\_enable\_classiclink) | A boolean flag to enable/disable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic | `bool` | `false` | no |
| <a name="input_enable_classiclink_dns_support"></a> [enable\_classiclink\_dns\_support](#input\_enable\_classiclink\_dns\_support) | A boolean flag to enable/disable ClassicLink DNS Support for the VPC | `bool` | `false` | no |
| <a name="input_enable_dns_hostnames"></a> [enable\_dns\_hostnames](#input\_enable\_dns\_hostnames) | A boolean flag to enable/disable DNS hostnames in the VPC | `bool` | `true` | no |
| <a name="input_enable_dns_support"></a> [enable\_dns\_support](#input\_enable\_dns\_support) | A boolean flag to enable/disable DNS support in the VPC. | `bool` | `true` | no |
| <a name="input_instance_tenancy"></a> [instance\_tenancy](#input\_instance\_tenancy) | A tenancy option for instances launched into the VPC | `string` | `"default"` | no |
| <a name="input_resource_prefix"></a> [resource\_prefix](#input\_resource\_prefix) | prefix for the created resources | `string` | n/a | yes |
| <a name="input_route_table_config"></a> [route\_table\_config](#input\_route\_table\_config) | details of route tables to be created | `any` | `null` | no |
| <a name="input_security_group_details"></a> [security\_group\_details](#input\_security\_group\_details) | details of security groups to be created | `any` | `null` | no |
| <a name="input_subnet_details"></a> [subnet\_details](#input\_subnet\_details) | details of subnets to be created | `any` | `null` | no |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | inique vpc identifying name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_route_table_id_list"></a> [route\_table\_id\_list](#output\_route\_table\_id\_list) | list of IDs of the subnets created |
| <a name="output_security_group_id_list"></a> [security\_group\_id\_list](#output\_security\_group\_id\_list) | list of IDs of the security groups created |
| <a name="output_subnet_id_list"></a> [subnet\_id\_list](#output\_subnet\_id\_list) | list of IDs of the subnets created |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | n/a |

## hcl .tfvars file format

```
assign_generated_ipv6_cidr_block = false
aws_region                       = ""
cidr_block                       = ""
common_tags                      = ""
enable_classiclink               = false
enable_classiclink_dns_support   = false
enable_dns_hostnames             = true
enable_dns_support               = true
instance_tenancy                 = "default"
resource_prefix                  = ""
route_table_config               = ""
security_group_details           = ""
subnet_details                   = ""
vpc_name                         = ""

```
