resource "aws_vpc" "vpc" {
  cidr_block                       = var.cidr_block
  instance_tenancy                 = var.instance_tenancy
  enable_dns_support               = var.enable_dns_support
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_classiclink               = var.enable_classiclink
  enable_classiclink_dns_support   = var.enable_classiclink_dns_support
  assign_generated_ipv6_cidr_block = var.assign_generated_ipv6_cidr_block

  tags = merge(
    var.common_tags,
    tomap(
      {
        "AWSResourceType" = "VPC",
        "Region"          = var.aws_region,
        "Name"            = format("%s-%s-vpc", var.resource_prefix, var.vpc_name)
      }
    )
  )

}

module "subnet" {
  for_each           = var.subnet_details != null ? var.subnet_details : {}
  source             = "../subnet/"
  subnet_list        = each.value
  vpc_id             = aws_vpc.vpc.id
  resource_prefix    = var.resource_prefix
  tags               = var.common_tags
  create_route_table = true

}
module "security_group" {
  source                 = "../security_group/"
  security_group_configs = var.security_group_details != null ? var.security_group_details : {}
  resource_prefix        = var.resource_prefix
  vpc_id                 = aws_vpc.vpc.id
  tags                   = var.common_tags
}