output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "subnet_id_list" {
  description = "list of IDs of the subnets created"
  value = {

    for k, value in module.subnet : k => value.subnet_id_list
  }
}

output "route_table_id_list" {
  description = "list of IDs of the subnets created"
  value = {

    for k, value in module.subnet : k => value.route_table_id_list
  }
}

output "security_group_id_list" {
  description = "list of IDs of the security groups created"
  value       = module.security_group.security_group_id_list
}