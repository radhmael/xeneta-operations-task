variable "aws_region" {
  description = "aws region where resources are being created in"
  type        = string
}

variable "resource_prefix" {
  description = "prefix for the created resources"
  type        = string
}

variable "vpc_name" {
  description = "inique vpc identifying name"
  type        = string
}

variable "cidr_block" {
  description = "CIDR block of the VPC"
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "enable_dns_support" {
  description = "A boolean flag to enable/disable DNS support in the VPC."
  default     = true
}

variable "enable_dns_hostnames" {
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = true
}

variable "enable_classiclink" {
  description = "A boolean flag to enable/disable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic"
  default     = false
}

variable "enable_classiclink_dns_support" {
  description = "A boolean flag to enable/disable ClassicLink DNS Support for the VPC"
  default     = false
}

variable "assign_generated_ipv6_cidr_block" {
  description = "Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify the range of IP addresses, or the size of the CIDR block"
  default     = false
}

variable "common_tags" {
  description = "a map of the tags to be attached"
  type        = map(string)
}

variable "subnet_details" {
  description = "details of subnets to be created"
  default     = null
}

variable "security_group_details" {
  description = "details of security groups to be created"
  default     = null
}

variable "route_table_config" {
  description = "details of route tables to be created"
  default     = null
}