# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.31.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_aws_ecr_repository"></a> [aws\_ecr\_repository](#module\_aws\_ecr\_repository) | ../../modules/aws_ecr_repository/ | n/a |
| <a name="module_aws_rds_databse"></a> [aws\_rds\_databse](#module\_aws\_rds\_databse) | ../../modules/database_resources/ | n/a |
| <a name="module_ecs"></a> [ecs](#module\_ecs) | ../../modules/ecs/ | n/a |
| <a name="module_task-definition"></a> [task-definition](#module\_task-definition) | ../../modules/task-definitions/ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/caller_identity) | data source |
| [aws_iam_account_alias.current](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_account_alias) | data source |
| [aws_iam_policy_document.aws_ecr_repository_policy](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.ecs_task_assume_role](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.secret_manager_access_policy](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_policy_document) | data source |
| [aws_security_groups.database_security_groups](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/security_groups) | data source |
| [aws_subnet.database_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/subnet) | data source |
| [aws_subnet_ids.database_subnet_ids](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/subnet_ids) | data source |
| [aws_vpc.selected_vpc](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_id"></a> [application\_id](#input\_application\_id) | n/a | `any` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | n/a | `any` | n/a | yes |
| <a name="input_cluster_details"></a> [cluster\_details](#input\_cluster\_details) | details to create ECS cluster | <pre>list(object({<br>    name                          = string<br>    logging                       = string<br>    container_insights_setting    = string<br>    default_ecs_capacity_provider = string<br>    ecs_capacity_providers        = list(string)<br>  }))</pre> | n/a | yes |
| <a name="input_cost_center"></a> [cost\_center](#input\_cost\_center) | n/a | `any` | n/a | yes |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | CPU milicores needed to run the conatiner | `any` | n/a | yes |
| <a name="input_data_classification"></a> [data\_classification](#input\_data\_classification) | n/a | `any` | n/a | yes |
| <a name="input_databse_details"></a> [databse\_details](#input\_databse\_details) | details to create ECS cluster | <pre>list(object({<br>    name                     = string<br>    vpc_name                 = string<br>    allocated_storage        = number<br>    backup_retention_period  = number<br>    backup_window            = string<br>    delete_automated_backups = bool<br>    engine                   = string<br>    engine_version           = string<br>    instance_class           = string<br>    maintenance_window       = string<br>    publicly_accessible      = bool<br>    skip_final_snapshot      = bool<br>    storage_encrypted        = bool<br>    username                 = string<br>    read_replica_count       = number<br>  }))</pre> | n/a | yes |
| <a name="input_db_init_container_definition_name"></a> [db\_init\_container\_definition\_name](#input\_db\_init\_container\_definition\_name) | Task definition name of the db init job | `string` | n/a | yes |
| <a name="input_db_init_task_name"></a> [db\_init\_task\_name](#input\_db\_init\_task\_name) | Name of the db init application task definition | `string` | n/a | yes |
| <a name="input_department"></a> [department](#input\_department) | n/a | `any` | n/a | yes |
| <a name="input_ecr_details"></a> [ecr\_details](#input\_ecr\_details) | details to create an Elastic Container Registry Repository | <pre>list(object({<br>    name                 = string<br>    image_tag_mutability = string<br>    force_delete         = bool<br>    scan_on_push         = bool<br>  }))</pre> | n/a | yes |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory limit allocated to the container | `any` | n/a | yes |
| <a name="input_owner"></a> [owner](#input\_owner) | n/a | `any` | n/a | yes |
| <a name="input_rates_app_task_name"></a> [rates\_app\_task\_name](#input\_rates\_app\_task\_name) | Name of the rates application task definition | `string` | n/a | yes |
| <a name="input_rates_container_definition_name"></a> [rates\_container\_definition\_name](#input\_rates\_container\_definition\_name) | Image name of the rates app | `string` | n/a | yes |
| <a name="input_rates_container_port"></a> [rates\_container\_port](#input\_rates\_container\_port) | rates-application container port | `number` | n/a | yes |
| <a name="input_skip_destroy"></a> [skip\_destroy](#input\_skip\_destroy) | Whether to skip destroying previous version when creating a new one | `bool` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_ecs_cluster_capacity_provider_name"></a> [aws\_ecs\_cluster\_capacity\_provider\_name](#output\_aws\_ecs\_cluster\_capacity\_provider\_name) | Details of the aws\_ecs\_cluster\_details |
| <a name="output_aws_ecs_cluster_details"></a> [aws\_ecs\_cluster\_details](#output\_aws\_ecs\_cluster\_details) | Details of the aws\_ecs\_cluster\_details |
| <a name="output_cloudwatch_log_group_details"></a> [cloudwatch\_log\_group\_details](#output\_cloudwatch\_log\_group\_details) | Details of the cloudwatch log group ccreated |
| <a name="output_database_endpoint"></a> [database\_endpoint](#output\_database\_endpoint) | n/a |
| <a name="output_databse_kms_key_id"></a> [databse\_kms\_key\_id](#output\_databse\_kms\_key\_id) | KMS key ID used to encrypt the DB with |
| <a name="output_execution_role_details"></a> [execution\_role\_details](#output\_execution\_role\_details) | n/a |
| <a name="output_registry_id_list"></a> [registry\_id\_list](#output\_registry\_id\_list) | The registry ID where the repository was created. |
| <a name="output_repository_url_list"></a> [repository\_url\_list](#output\_repository\_url\_list) | The URL of the repository |

## hcl .tfvars file format

```
application_id                    = ""
aws_region                        = ""
cluster_details                   = ""
cost_center                       = ""
cpu                               = ""
data_classification               = ""
databse_details                   = ""
db_init_container_definition_name = ""
db_init_task_name                 = ""
department                        = ""
ecr_details                       = ""
memory                            = ""
owner                             = ""
rates_app_task_name               = ""
rates_container_definition_name   = ""
rates_container_port              = ""
skip_destroy                      = ""

```
