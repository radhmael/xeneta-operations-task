data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "aws_ecr_repository_policy" {
  statement {
    sid    = "ecr_policy"
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS", data.aws_caller_identity.current.account_id)
      ]
    }

    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:DescribeRepositories",
      "ecr:GetRepositoryPolicy",
      "ecr:ListImages",
      "ecr:DeleteRepository",
      "ecr:BatchDeleteImage",
      "ecr:SetRepositoryPolicy",
      "ecr:DeleteRepositoryPolicy"
    ]

    resources = [
      "arn:aws:s3:::xeneta-*",
      "arn:aws:s3:::xeneta-*/*"
    ]
  }
}

#database

data "aws_vpc" "selected_vpc" {
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}

data "aws_subnet_ids" "database_subnet_ids" {
  vpc_id = data.aws_vpc.selected_vpc.id
  filter {
    name   = "tag:type"
    values = ["database"]
  }
}

data "aws_subnet" "database_subnet" {
  for_each = data.aws_subnet_ids.database_subnet_ids.ids
  id       = each.key
}

data "aws_security_groups" "database_security_groups" {
  tags = {
    Name = local.database_sg_regex
  }
}

data "aws_iam_policy_document" "secret_manager_access_policy" {
  statement {
    sid    = "AllowROaccessToECSRoles"
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = [
        format("arn:aws:iam::%s:role/%s-ecs_task_execution_role-iam-role", data.aws_caller_identity.current.account_id, local.resource_prefix),
        format("arn:aws:iam::%s:role/%s-ecs_task_role-iam-role", data.aws_caller_identity.current.account_id, local.resource_prefix)
      ]
    }

    actions = [
      "secretsmanager:GetSecretValue"
    ]

    resources = [
      "*"
    ]
  }
}
## task definition

data "aws_iam_policy_document" "ecs_task_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
