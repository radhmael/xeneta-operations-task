locals {
  account_name        = data.aws_iam_account_alias.current.account_alias
  account_name_list   = split("-", local.account_name)
  business_unit       = local.account_name_list[1]
  project_name        = local.account_name_list[2]
  environment         = local.account_name_list[3]
  resource_prefix     = format("%s-%s-%s", local.business_unit, local.project_name, local.environment)
  vpc_name            = format("%s-%s-vpc", local.resource_prefix, var.application_id)
  database_sg_regex   = format("%s-database-security-group", local.resource_prefix)
  database_subnet_azs = [for subnet in data.aws_subnet.database_subnet : subnet.availability_zone]
  common_tags = {
    "application_id" : var.application_id,
    "cost_center" : var.cost_center,
    "department" : local.project_name,
    "data_classification" : var.data_classification,
    "region" : var.aws_region,
    "owner" : var.owner,
    "environment" : local.environment
  }

  #task def

  ecs_execution_role_policies = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
  ]

  iam_roles_for_task_execution = [
    {
      role_name = "ecs_task_execution_role",
      polies_to_attach = [
        "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
      ],
      assume_role_policy = data.aws_iam_policy_document.ecs_task_assume_role.json
    },
    {
      role_name = "ecs_task_role",
      polies_to_attach = [
        "arn:aws:iam::aws:policy/AmazonRDSFullAccess",
        "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
      ],
      assume_role_policy = data.aws_iam_policy_document.ecs_task_assume_role.json
    }
  ]

  repository_url_map = { for name, url in flatten(values(module.aws_ecr_repository)[*].repository_url) : url.name => url.url }

}