# ECR module
module "aws_ecr_repository" {
  source                    = "../../modules/aws_ecr_repository/"
  for_each                  = { for ecr in var.ecr_details : ecr.name => ecr }
  container_repository_name = format("%s-%s", local.resource_prefix, each.value.name)
  image_tag_mutability      = each.value.image_tag_mutability
  scan_on_push              = each.value.scan_on_push
  force_delete              = each.value.force_delete
  common_tags               = local.common_tags
  aws_ecr_repository_policy = data.aws_iam_policy_document.aws_ecr_repository_policy.json
}

# ECS module
module "ecs" {
  source                        = "../../modules/ecs/"
  for_each                      = { for cluster in var.cluster_details : cluster.name => cluster }
  cluster_name                  = each.value.name
  resource_prefix               = local.resource_prefix
  common_tags                   = local.common_tags
  logging                       = each.value.logging
  container_insights_setting    = each.value.container_insights_setting
  ecs_capacity_providers        = each.value.ecs_capacity_providers
  default_ecs_capacity_provider = each.value.default_ecs_capacity_provider
}

# RDS module
module "aws_rds_databse" {
  for_each = { for databse in var.databse_details : databse.name => databse }

  source                       = "../../modules/database_resources/"
  resource_prefix              = local.resource_prefix
  databse_name                 = each.value.name
  common_tags                  = local.common_tags
  allocated_storage            = each.value.allocated_storage
  availability_zone_list       = local.database_subnet_azs
  backup_retention_period      = each.value.backup_retention_period
  backup_window                = each.value.backup_window
  delete_automated_backups     = each.value.delete_automated_backups
  engine                       = each.value.engine
  engine_version               = each.value.engine_version
  identifier                   = format("%s-databse", local.resource_prefix)
  instance_class               = each.value.instance_class
  maintenance_window           = each.value.maintenance_window
  publicly_accessible          = each.value.publicly_accessible
  skip_final_snapshot          = each.value.skip_final_snapshot
  storage_encrypted            = each.value.storage_encrypted
  username                     = each.value.username
  vpc_security_group_ids       = data.aws_security_groups.database_security_groups.ids
  database_subnet_ids          = data.aws_subnet_ids.database_subnet_ids.ids
  read_replica_count           = each.value.read_replica_count
  secret_manager_access_policy = data.aws_iam_policy_document.secret_manager_access_policy.json
}

# Task definitions for applications
module "task-definition" {
  source                            = "../../modules/task-definitions/"
  resource_prefix                   = local.resource_prefix
  common_tags                       = local.common_tags
  roles_to_create                   = local.iam_roles_for_task_execution
  db_secret_name_arn                = values(module.aws_rds_databse)[0]["database_password_arn"]
  rates_app_task_name               = var.rates_app_task_name
  rates_container_definition_name   = var.rates_container_definition_name
  rates_container_port              = var.rates_container_port
  db_init_task_name                 = var.db_init_task_name
  db_init_container_definition_name = var.db_init_container_definition_name
  cpu                               = var.cpu
  memory                            = var.memory
  skip_destroy                      = var.skip_destroy
  cloudwatch_log_group_name         = values(module.ecs)[0]["cloudwatch_log_group_details"]["name"]
  aws_region                        = var.aws_region
  repository_url_map                = local.repository_url_map
}