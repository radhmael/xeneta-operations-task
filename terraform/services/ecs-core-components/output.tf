##ECR

output "registry_id_list" {
  description = "The registry ID where the repository was created."
  value       = values(module.aws_ecr_repository)[*].registry_id

}
output "repository_url_list" {
  description = " The URL of the repository"
  value       = values(module.aws_ecr_repository)[*].repository_url
}

##ECS

output "cloudwatch_log_group_details" {
  description = "Details of the cloudwatch log group ccreated"
  value       = values(module.ecs)[*].cloudwatch_log_group_details

}
output "aws_ecs_cluster_details" {
  description = "Details of the aws_ecs_cluster_details"
  value       = values(module.ecs)[*].aws_ecs_cluster_details
}
output "aws_ecs_cluster_capacity_provider_name" {
  description = "Details of the aws_ecs_cluster_details"
  value       = values(module.ecs)[*].aws_ecs_cluster_capacity_provider_name
}


output "databse_kms_key_id" {
  description = "KMS key ID used to encrypt the DB with"
  value       = values(module.aws_rds_databse)[*].kms_key_id
}

output "execution_role_details" {
  value = module.task-definition[*].execution_role_details
}
output "database_endpoint" {
  value = module.aws_rds_databse[*].*
}


