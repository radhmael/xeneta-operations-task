variable "application_id" {}

variable "cost_center" {}
variable "department" {}
variable "data_classification" {}
variable "owner" {}

variable "aws_region" {}


# ******** ECR module ********
####################################################################

# ECR attributes
variable "ecr_details" {
  description = "details to create an Elastic Container Registry Repository"
  type = list(object({
    name                 = string
    image_tag_mutability = string
    force_delete         = bool
    scan_on_push         = bool
  }))

}

variable "cluster_details" {
  description = "details to create ECS cluster"
  type = list(object({
    name                          = string
    logging                       = string
    container_insights_setting    = string
    default_ecs_capacity_provider = string
    ecs_capacity_providers        = list(string)
  }))
}

variable "databse_details" {
  description = "details to create ECS cluster"
  type = list(object({
    name                     = string
    vpc_name                 = string
    allocated_storage        = number
    backup_retention_period  = number
    backup_window            = string
    delete_automated_backups = bool
    engine                   = string
    engine_version           = string
    instance_class           = string
    maintenance_window       = string
    publicly_accessible      = bool
    skip_final_snapshot      = bool
    storage_encrypted        = bool
    username                 = string
    read_replica_count       = number
  }))
}



# ******** Task definition module ********
###############################################################
# rates-application task definition attributes
variable "rates_app_task_name" {
  description = "Name of the rates application task definition"
  type        = string
}

variable "rates_container_definition_name" {
  description = "Image name of the rates app"
  type        = string
}


variable "rates_container_port" {
  description = "rates-application container port"
  type        = number
}


###############################################################
# db-initialization task definition attributes

variable "db_init_task_name" {
  description = "Name of the db init application task definition"
  type        = string
}

variable "db_init_container_definition_name" {
  description = "Task definition name of the db init job"
  type        = string
}


###############################################################
# Common attributes of task-definition module

variable "cpu" {
  description = "CPU milicores needed to run the conatiner"
}

variable "memory" {
  description = "Memory limit allocated to the container"
}

variable "skip_destroy" {
  description = "Whether to skip destroying previous version when creating a new one"
  type        = bool
}

# variable "db_password_name" {
#   description = "Secret name for the secret manager to store db password"
#   type        = string
# }

# variable "db_endpoint_name" {
#   description = "Secret name for the secret manager to store db endpoint"
#   type        = string
# }
# variable "db_secret_name_prefix" {
#   description = "Name prefix of the secret manager secret name"
#   type = string
# }
