# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.31.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_alb"></a> [alb](#module\_alb) | ../../modules/load_balancer/ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_appautoscaling_policy.ecs_policy](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_target.rates-app-ecs-target](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/resources/appautoscaling_target) | resource |
| [aws_ecs_service.application_services](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/resources/ecs_service) | resource |
| [aws_service_discovery_service.discovery-service](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/resources/service_discovery_service) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/caller_identity) | data source |
| [aws_ecs_cluster.ecs-cluster](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/ecs_cluster) | data source |
| [aws_ecs_task_definition.db-init-application-task-definition](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/ecs_task_definition) | data source |
| [aws_ecs_task_definition.db-init-task-defintion](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/ecs_task_definition) | data source |
| [aws_ecs_task_definition.rates-application-task-definitio](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/ecs_task_definition) | data source |
| [aws_ecs_task_definition.rates-application-task-definition](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/ecs_task_definition) | data source |
| [aws_iam_account_alias.current](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_account_alias) | data source |
| [aws_security_groups.private_security_group_ids](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/security_groups) | data source |
| [aws_security_groups.public_security_groups](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/security_groups) | data source |
| [aws_service_discovery_dns_namespace.service_d_namespace](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/service_discovery_dns_namespace) | data source |
| [aws_subnet_ids.private_subnet_id_list](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/subnet_ids) | data source |
| [aws_subnet_ids.public_subnet_id_list](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/subnet_ids) | data source |
| [aws_vpc.selected_vpc](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_id"></a> [application\_id](#input\_application\_id) | n/a | `any` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | n/a | `any` | n/a | yes |
| <a name="input_cost_center"></a> [cost\_center](#input\_cost\_center) | n/a | `any` | n/a | yes |
| <a name="input_data_classification"></a> [data\_classification](#input\_data\_classification) | n/a | `any` | n/a | yes |
| <a name="input_department"></a> [department](#input\_department) | n/a | `any` | n/a | yes |
| <a name="input_ecs_autoscaling_details"></a> [ecs\_autoscaling\_details](#input\_ecs\_autoscaling\_details) | n/a | `any` | n/a | yes |
| <a name="input_ecs_service_details"></a> [ecs\_service\_details](#input\_ecs\_service\_details) | n/a | <pre>list(<br>    object({<br>      app_container_definition_name          = string<br>      app_container_port                     = number<br>      app_deployment_maximum_percent         = number<br>      app_deployment_minimum_healthy_percent = number<br>      app_desired_count                      = number<br>      service_name                           = string<br>      task_definition_name                   = string<br>      launch_type                            = string<br>      service_registries                     = number<br>      load_balancer                          = number<br>    })<br>  )</pre> | n/a | yes |
| <a name="input_load_balancer_details"></a> [load\_balancer\_details](#input\_load\_balancer\_details) | n/a | <pre>list(<br>    object({<br>      name                       = string<br>      internal                   = bool<br>      type                       = string<br>      enable_deletion_protection = bool<br>      listener_port              = number<br>      listener_protocol          = string<br>      listener_default_action    = string<br>      target_group_port          = number<br>      target_group_type          = string<br>      healthy_threshold          = number<br>      health_check_interval      = number<br>      health_check_port          = number<br>      health_check_path          = string<br>      health_check_protocol      = string<br>      health_check_timeout       = number<br>      unhealthy_threshold        = number<br>    })<br>  )</pre> | n/a | yes |
| <a name="input_owner"></a> [owner](#input\_owner) | n/a | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alb_dns_name"></a> [alb\_dns\_name](#output\_alb\_dns\_name) | DNS value for the load balancer |

## hcl .tfvars file format

```
application_id          = ""
aws_region              = ""
cost_center             = ""
data_classification     = ""
department              = ""
ecs_autoscaling_details = ""
ecs_service_details     = ""
load_balancer_details   = ""
owner                   = ""

```
