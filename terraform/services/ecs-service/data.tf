##common

data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "current" {}

data "aws_vpc" "selected_vpc" {
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}

##aws_service_discovery_service

data "aws_service_discovery_dns_namespace" "service_d_namespace" {
  name = format("%s-local", var.application_id)
  type = "DNS_PRIVATE"
}

## application load balancer

data "aws_subnet_ids" "public_subnet_id_list" {
  vpc_id = data.aws_vpc.selected_vpc.id
  filter {
    name   = "tag:type"
    values = ["public"]
  }
}

data "aws_security_groups" "public_security_groups" {
  tags = {
    Name = local.public_sg_regex
  }
}

data "aws_subnet_ids" "private_subnet_id_list" {
  vpc_id = data.aws_vpc.selected_vpc.id
  filter {
    name   = "tag:type"
    values = ["private"]
  }
}



data "aws_security_groups" "private_security_group_ids" {
  tags = {
    Name = local.private_sg_regex
  }
}


##ecs services

# Retrieve ECS cluster ID
data "aws_ecs_cluster" "ecs-cluster" {
  cluster_name = local.ecs_cluster_name
}

# Retrieve task definition information
data "aws_ecs_task_definition" "rates-application-task-definitio" {
  task_definition = local.rates_task_definition_name
}

data "aws_ecs_task_definition" "db-init-application-task-definition" {
  task_definition = local.db_init_task_definition_name
}

# Retrieve task definition information
data "aws_ecs_task_definition" "db-init-task-defintion" {
  task_definition = "db-init-application-task-definition"
}

data "aws_ecs_task_definition" "rates-application-task-definition" {
  task_definition = "rates-application-task-definition"
}