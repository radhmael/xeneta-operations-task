locals {
  account_name      = data.aws_iam_account_alias.current.account_alias
  account_name_list = split("-", local.account_name)
  business_unit     = local.account_name_list[1]
  project_name      = local.account_name_list[2]
  environment       = local.account_name_list[3]

  resource_prefix = format("%s-%s-%s", local.business_unit, local.project_name, local.environment)
  vpc_name        = format("%s-%s-vpc", local.resource_prefix, var.application_id)
  common_tags = {
    "application_id" : var.application_id,
    "cost_center" : var.cost_center,
    "department" : local.project_name,
    "data_classification" : var.data_classification,
    "region" : var.aws_region,
    "owner" : var.owner,
    "environment" : local.environment
  }

  public_sg_regex  = format("%s-public-security-group", local.resource_prefix)
  private_sg_regex = format("%s-private-security-group", local.resource_prefix)

  ecs_cluster_name             = format("%s-%s-cluster", local.resource_prefix, var.application_id)
  rates_task_definition_name   = "rates-application-task-definition"
  db_init_task_definition_name = "db-init-application-task-definition"
  service_details_map          = { for service in var.ecs_service_details : service.service_name => service }


}