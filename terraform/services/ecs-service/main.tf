module "alb" {
  for_each = { for lb in var.load_balancer_details : lb.name => lb }
  source   = "../../modules/load_balancer/"

  name = format("%s-%s-alb", local.resource_prefix, each.value.name)

  load_balancer_type = each.value.type

  vpc_id          = data.aws_vpc.selected_vpc.id
  security_groups = data.aws_security_groups.public_security_groups.ids
  subnets         = data.aws_subnet_ids.public_subnet_id_list.ids

  http_tcp_listeners = [
    # Forward action is default, either when defined or undefined
    {
      port               = each.value.listener_port
      protocol           = each.value.listener_protocol
      target_group_index = 0
      action_type        = each.value.listener_default_action
    }
  ]


  target_groups = [
    {
      name_prefix          = "h1"
      backend_protocol     = each.value.listener_protocol
      backend_port         = each.value.target_group_port
      target_type          = each.value.target_group_type
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = each.value.health_check_interval
        path                = each.value.health_check_path
        port                = each.value.health_check_port
        healthy_threshold   = each.value.healthy_threshold
        unhealthy_threshold = each.value.unhealthy_threshold
        timeout             = each.value.health_check_timeout
        protocol            = each.value.health_check_protocol
        matcher             = "200-399"
      }
      protocol_version = "HTTP1"
      tags = {
        InstanceTargetGroupTag = "baz"
      }
    }
  ]

  tags = local.common_tags

  lb_tags = {
    load_balancer = format("%s-%s-alb", local.resource_prefix, each.value.name)
  }
}

resource "aws_service_discovery_service" "discovery-service" {
  name = "rates-application-service"

  dns_config {
    namespace_id = data.aws_service_discovery_dns_namespace.service_d_namespace.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

}

resource "aws_ecs_service" "application_services" {
  for_each        = { for service in var.ecs_service_details : service.service_name => service }
  name            = each.value.service_name
  cluster         = data.aws_ecs_cluster.ecs-cluster.id
  task_definition = each.value.task_definition_name == "rates-application-task-definition" ? data.aws_ecs_task_definition.rates-application-task-definition.arn : data.aws_ecs_task_definition.db-init-task-defintion.arn
  desired_count   = each.value.app_desired_count

  dynamic "load_balancer" {
    for_each = each.value.load_balancer == null ? toset([]) : toset([1])
    content {
      target_group_arn = values(module.alb)[0].target_group_arns[0]
      container_name   = each.value.app_container_definition_name
      container_port   = each.value.app_container_port

    }
  }

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }

  network_configuration {
    subnets          = data.aws_subnet_ids.private_subnet_id_list.ids
    assign_public_ip = false
    security_groups  = data.aws_security_groups.private_security_group_ids.ids
  }

  dynamic "service_registries" {
    for_each = each.value.service_registries == null ? toset([]) : toset([1])
    content {
      registry_arn = aws_service_discovery_service.discovery-service.arn
    }
  }

  deployment_maximum_percent         = each.value.app_deployment_maximum_percent
  deployment_minimum_healthy_percent = each.value.app_deployment_minimum_healthy_percent
  launch_type                        = each.value.launch_type

}


# ECS rates-application autoscaling
resource "aws_appautoscaling_target" "rates-app-ecs-target" {
  max_capacity       = var.ecs_autoscaling_details["max_capacity"]
  min_capacity       = var.ecs_autoscaling_details["min_capacity"]
  resource_id        = format("service/%s/%s", local.ecs_cluster_name, var.ecs_autoscaling_details["name"])
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

}

# rates-application autoscaling policy
resource "aws_appautoscaling_policy" "ecs_policy" {
  name               = var.ecs_autoscaling_details["target_tracking_policy_name"]
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.rates-app-ecs-target.resource_id
  scalable_dimension = aws_appautoscaling_target.rates-app-ecs-target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.rates-app-ecs-target.service_namespace

  target_tracking_scaling_policy_configuration {
    target_value      = var.ecs_autoscaling_details["cpu_threshold"]
    scale_in_cooldown = var.ecs_autoscaling_details["cool_down_time"]
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }
}