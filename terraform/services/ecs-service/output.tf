output "alb_dns_name" {
  description = "DNS value for the load balancer"
  value       = module.alb[*].*
}