variable "application_id" {}

variable "cost_center" {}
variable "department" {}
variable "data_classification" {}
variable "owner" {}

variable "aws_region" {}

variable "ecs_service_details" {
  type = list(
    object({
      app_container_definition_name          = string
      app_container_port                     = number
      app_deployment_maximum_percent         = number
      app_deployment_minimum_healthy_percent = number
      app_desired_count                      = number
      service_name                           = string
      task_definition_name                   = string
      launch_type                            = string
      service_registries                     = number
      load_balancer                          = number
    })
  )
}
variable "load_balancer_details" {
  type = list(
    object({
      name                       = string
      internal                   = bool
      type                       = string
      enable_deletion_protection = bool
      listener_port              = number
      listener_protocol          = string
      listener_default_action    = string
      target_group_port          = number
      target_group_type          = string
      healthy_threshold          = number
      health_check_interval      = number
      health_check_port          = number
      health_check_path          = string
      health_check_protocol      = string
      health_check_timeout       = number
      unhealthy_threshold        = number
    })
  )
}

variable "ecs_autoscaling_details" {

}