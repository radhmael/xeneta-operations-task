# 

## Module Development Standards

[Please refer the development Standards before working on a repo](https://developer.hashicorp.com/terraform/language/modules/develop/structure)

 ## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.31.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.31.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_internet_gateway"></a> [internet\_gateway](#module\_internet\_gateway) | ../../modules/internet_gateway | n/a |
| <a name="module_nat_gateway"></a> [nat\_gateway](#module\_nat\_gateway) | ../../modules/nat_gateway | n/a |
| <a name="module_route_table_association"></a> [route\_table\_association](#module\_route\_table\_association) | ../../modules/route_table_association | n/a |
| <a name="module_security_group_rules"></a> [security\_group\_rules](#module\_security\_group\_rules) | ../../modules/security_group_rule_list | n/a |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | ../../modules/vpc | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_service_discovery_private_dns_namespace.service_d_namespace](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/resources/service_discovery_private_dns_namespace) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/availability_zones) | data source |
| [aws_iam_account_alias.current](https://registry.terraform.io/providers/hashicorp/aws/4.31.0/docs/data-sources/iam_account_alias) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_id"></a> [application\_id](#input\_application\_id) | ID of the application. Will be used to name core resources | `any` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | region where resources wiill be created | `any` | n/a | yes |
| <a name="input_cost_center"></a> [cost\_center](#input\_cost\_center) | the cost center for charge back | `any` | n/a | yes |
| <a name="input_data_classification"></a> [data\_classification](#input\_data\_classification) | data classification for data inside the resource | `any` | n/a | yes |
| <a name="input_department"></a> [department](#input\_department) | department consuming the resources | `any` | n/a | yes |
| <a name="input_owner"></a> [owner](#input\_owner) | contact email for resource | `any` | n/a | yes |
| <a name="input_vpc_details"></a> [vpc\_details](#input\_vpc\_details) | details of the VPC to be created. Must follow the following format | <pre>set(object({<br>    vpc_name       = string<br>    cidr_range     = string<br>    subnet_configs = map(list(any))<br>    security_group_configs = map(list(object({<br>      type                     = string<br>      from_port                = number<br>      to_port                  = number<br>      protocol                 = string<br>      cidr_blocks              = list(string)<br>      ipv6_cidr_blocks         = list(string)<br>      description              = string<br>      self                     = bool<br>      source_security_group_id = string<br>    })))<br>    igw_required         = bool<br>    route_table_configs  = list(any)<br>    enable_dns_hostnames = bool<br>    # route_table_association_configs = list(map(string))<br>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_eip_allocation_id"></a> [eip\_allocation\_id](#output\_eip\_allocation\_id) | list of allocation IDs for the eips created |
| <a name="output_nat_gateway_id"></a> [nat\_gateway\_id](#output\_nat\_gateway\_id) | list of IDs for the NAT GWs created |
| <a name="output_route_table_id_list"></a> [route\_table\_id\_list](#output\_route\_table\_id\_list) | list of IDs of the route tablesye created |
| <a name="output_security_group_id_list"></a> [security\_group\_id\_list](#output\_security\_group\_id\_list) | list of IDs of the route tablesye created |
| <a name="output_subnet_id_list"></a> [subnet\_id\_list](#output\_subnet\_id\_list) | list of IDs of the subnets created |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | VPC ID of the resource |

## hcl .tfvars file format

```
application_id      = ""
aws_region          = ""
cost_center         = ""
data_classification = ""
department          = ""
owner               = ""
vpc_details         = ""

```
