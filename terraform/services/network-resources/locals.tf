locals {

  ##common resource values
  account_name     = split("-", data.aws_iam_account_alias.current.account_alias)
  account_category = local.account_name[1]
  project_name     = local.account_name[2]
  environment      = local.account_name[3]
  resource_prefix  = format("%s-%s-%s", local.account_category, local.project_name, local.environment)

  namespace_tags = {
    "Name" = format("%s-ecs-namespace", local.resource_prefix)
  }

  common_tags = {
    "application_id" : var.application_id,
    "cost_center" : var.cost_center,
    "department" : local.project_name,
    "data_classification" : var.data_classification,
    "region" : var.aws_region,
    "owner" : var.owner,
    "environment" : local.environment
  }

  subnet_id_list      = values(module.vpc)[*].subnet_id_list
  route_table_id_list = values(module.vpc)[*].route_table_id_list
}