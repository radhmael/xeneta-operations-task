module "vpc" {
  for_each               = { for vpc in var.vpc_details : vpc.cidr_range => vpc }
  source                 = "../../modules/vpc"
  vpc_name               = each.value.vpc_name
  cidr_block             = each.value.cidr_range
  common_tags            = local.common_tags
  aws_region             = var.aws_region
  enable_dns_hostnames   = each.value.enable_dns_hostnames
  subnet_details         = each.value.subnet_configs
  security_group_details = each.value.security_group_configs
  resource_prefix        = local.resource_prefix
}

module "route_table_association" {
  source              = "../../modules/route_table_association"
  for_each            = local.subnet_id_list[0]
  subnet_id_list      = local.subnet_id_list[0][each.key]
  route_table_id_list = local.route_table_id_list[0][each.key]
}

module "internet_gateway" {
  for_each                   = { for k, value in module.vpc : k => value.vpc_id }
  source                     = "../../modules/internet_gateway"
  vpc_id                     = each.value
  public_route_table_id_list = local.route_table_id_list[0]["public"]
  common_tags                = local.common_tags
}

module "nat_gateway" {
  for_each                    = local.subnet_id_list[0]["public"]
  source                      = "../../modules/nat_gateway"
  resource_prefix             = local.resource_prefix
  subnet_id                   = each.value
  subnet_name                 = each.key
  private_route_table_details = local.route_table_id_list[0]["private"]
  common_tags                 = local.common_tags
}

resource "aws_service_discovery_private_dns_namespace" "service_d_namespace" {
  for_each    = { for k, value in module.vpc : k => value.vpc_id }
  name        = format("%s-local", var.application_id)
  description = "Private hosted zone for service discovery"
  vpc         = each.value

  tags = local.common_tags
}

module "security_group_rules" {
  for_each               = { for vpc in var.vpc_details : vpc.cidr_range => vpc }
  source                 = "../../modules/security_group_rule_list"
  resource_prefix        = local.resource_prefix
  security_group_details = each.value.security_group_configs
  security_group_id_list = values(module.vpc)[*].security_group_id_list
}