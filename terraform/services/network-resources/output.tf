output "vpc_id" {
  description = "VPC ID of the resource"
  value = {
    for k, value in module.vpc : k => value.vpc_id
  }
}

output "subnet_id_list" {
  description = "list of IDs of the subnets created"
  value       = values(module.vpc)[*].subnet_id_list
}

output "route_table_id_list" {
  description = "list of IDs of the route tablesye created"
  value       = values(module.vpc)[*].route_table_id_list
}

output "security_group_id_list" {
  description = "list of IDs of the route tablesye created"
  value       = values(module.vpc)[*].security_group_id_list
}

output "eip_allocation_id" {

  description = "list of allocation IDs for the eips created "
  value       = values(module.nat_gateway)[*].eip_allocation_id
}

output "nat_gateway_id" {

  description = "list of IDs for the NAT GWs created "
  value       = values(module.nat_gateway)[*].nat_gateway_id
}
