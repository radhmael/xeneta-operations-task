variable "vpc_details" {
  description = "details of the VPC to be created. Must follow the following format"
  type = set(object({
    vpc_name       = string
    cidr_range     = string
    subnet_configs = map(list(any))
    security_group_configs = map(list(object({
      type                     = string
      from_port                = number
      to_port                  = number
      protocol                 = string
      cidr_blocks              = list(string)
      ipv6_cidr_blocks         = list(string)
      description              = string
      self                     = bool
      source_security_group_id = string
    })))
    igw_required         = bool
    route_table_configs  = list(any)
    enable_dns_hostnames = bool
    # route_table_association_configs = list(map(string))
  }))

}

variable "application_id" {
  description = "ID of the application. Will be used to name core resources"
}

variable "cost_center" {
  description = "the cost center for charge back"
}

variable "department" {
  description = "department consuming the resources"
}

variable "data_classification" {
  description = "data classification for data inside the resource"
}

variable "owner" {
  description = "contact email for resource"
}

variable "aws_region" {

  description = "region where resources wiill be created"
}